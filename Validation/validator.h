#ifndef VALIDATOR_H
#define VALIDATOR_H

#include <QString>
#include <QDebug>
#include <QRegularExpression>
#include <vector>
#include <set>

#include "Validation_global.h"

#include <iostream>


class VALIDATION_EXPORT IValidator
{
public:
    IValidator() = default;
    virtual bool Validate(const QString& text) = 0;
    virtual QString Error() = 0;
};

class VALIDATION_EXPORT Validator: public IValidator
{
public:
    bool Validate(const QString &text) override;
    QString Error() override;
};

class VALIDATION_EXPORT RequiredValidator: public IValidator
{
public:
    bool Validate(const QString &text) override;
    QString Error() override;
};

class VALIDATION_EXPORT RegexValidator: public IValidator
{
public:
    RegexValidator(const QRegularExpression& pattern);
    bool Validate(const QString& text) override;
    QString Error() override;

private:
    QRegularExpression m_pattern;
};


class VALIDATION_EXPORT ContainsValidator: public IValidator
{
public:
    enum class Operation { POSITIVE, NEGATIVE };
    ContainsValidator(const QString& characters, const Operation op = Operation::POSITIVE);
    bool Validate(const QString &text) override;
    QString Error() override;

    Operation m_operation;
    QString m_data;

};

class ValidatorBuilder;

class VALIDATION_EXPORT ComposedValidator: public IValidator
{
public:
    ComposedValidator();
    bool Validate(const QString &text) override;
    QString Error() override;
    void addSubvalidator(std::shared_ptr<IValidator> validator);
    void SetRequired(const bool required);
    static ValidatorBuilder create();

protected:
    std::vector<std::shared_ptr<IValidator>> m_validators;
    QString m_error;
    bool m_required;
};

class VALIDATION_EXPORT ValidatorBuilder
{
    ComposedValidator m_validator;
public:
    operator ComposedValidator();

    ValidatorBuilder& required();
    ValidatorBuilder& contains(const QString& pattern);
    ValidatorBuilder& invalidCharacters(const QString& characters);
    ValidatorBuilder& validCharacters(const QString& characters);
    ValidatorBuilder& matchRegex(const QRegularExpression& pattern);


};


#endif // VALIDATOR_H
