#include "validator.h"

ComposedValidator::ComposedValidator(): m_required(false)
{
    qDebug() << "Reqq: " << m_required;
}

bool ComposedValidator::Validate(const QString &text)
{
    m_error.clear();

    if (m_required && text.isEmpty())
    {
        m_error = "This value is required!";
        return false;
    }

    if (!m_required && text.isEmpty())
    {
        return true;
    }

    auto actualResult = false;

    for(auto& validator: m_validators)
    {
        actualResult = validator->Validate(text);

        if (!actualResult)
        {
            m_error = validator->Error();
            break;
        }
    }
    return actualResult;
}

QString ComposedValidator::Error()
{
    return m_error;
}

void ComposedValidator::addSubvalidator(std::shared_ptr<IValidator> validator)
{
    m_validators.push_back(validator);
}

void ComposedValidator::SetRequired(const bool required)
{
    m_required = required;
}

ValidatorBuilder ComposedValidator::create()
{
    return ValidatorBuilder();
}

ValidatorBuilder::operator ComposedValidator()
{
    return m_validator;
}

ValidatorBuilder &ValidatorBuilder::required()
{
    m_validator.SetRequired(true);
    return *this;
}

ValidatorBuilder &ValidatorBuilder::contains(const QString &pattern)
{
    m_validator.addSubvalidator(std::make_shared<ContainsValidator>(pattern));
    return *this;
}

ValidatorBuilder &ValidatorBuilder::invalidCharacters(const QString &characters)
{
    m_validator.addSubvalidator(
        std::make_shared<ContainsValidator>(
            characters, ContainsValidator::Operation::NEGATIVE
        )
    );
    return *this;
}

ValidatorBuilder &ValidatorBuilder::validCharacters(const QString &characters)
{
    m_validator.addSubvalidator(
        std::make_shared<ContainsValidator>(
            characters, ContainsValidator::Operation::POSITIVE
        )
    );
    return *this;
}

ValidatorBuilder &ValidatorBuilder::matchRegex(const QRegularExpression &pattern)
{
    m_validator.addSubvalidator(std::make_shared<RegexValidator>(pattern));
    return *this;
}

bool RequiredValidator::Validate(const QString &text)
{
    return !text.isEmpty();
}

QString RequiredValidator::Error()
{
    return "This value is required!";
}

ContainsValidator::ContainsValidator(const QString &characters, Operation operation)
    : m_operation(operation), m_data(characters)
{
}

bool ContainsValidator::Validate(const QString &text)
{
    auto result = true;

    for (const auto c: m_data)
    {
        if (!text.contains(c))
            result = false;
    }

    return m_operation == Operation::POSITIVE ? result : !result;
}

QString ContainsValidator::Error()
{
    QString result = "Value ";
    result += m_operation == Operation::POSITIVE
            ? "does not contain required "
            : "contains invalid ";
    result += "characters: '" + m_data + "'";

    return result;
}

bool Validator::Validate(const QString & /*text*/)
{
    return true;
}

QString Validator::Error()
{
    return QString();
}

RegexValidator::RegexValidator(const QRegularExpression &pattern)
    : m_pattern(pattern)
{
}

bool RegexValidator::Validate(const QString &text)
{
    const auto match = m_pattern.match(text);
    return match.hasMatch();
}

QString RegexValidator::Error()
{
    QString result = "Value does not match '" + m_pattern.pattern() + "'";
    return result;
}
