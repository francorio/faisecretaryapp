#ifndef GENERATORLOGIC_H
#define GENERATORLOGIC_H

#include <vector>
#include "GeneratorLogic_global.h"

#include "Utils/enummapper.h"
#include "Models/workitem.h"
#include "Models/studygroup.h"
#include "Models/subject.h"

class GENERATORLOGIC_EXPORT GeneratorLogic
{
public:
    static std::vector<WorkItem> Generate(
        const StudyGroup& group,
        const std::vector<Subject>& subjects,
        const std::vector<WorkItem>& generated = std::vector<WorkItem>()
    );

private:
    static QString GenerateName(const WorkItem& wi, const Subject& sub);
    static bool ExistsItem(const std::vector<WorkItem>& items, const WorkItemType t);
    static std::vector<WorkItem> GetItems(const std::vector<WorkItem>& items, const WorkItemType t);
    static std::vector<int> SplitToGroups(const int enrolled, const int capacity);
    static void GenerateLectureWorkItems(const Subject& subject, const StudyGroup& group, const std::vector<WorkItem>& generated, std::vector<WorkItem>& result);
    static void GenerateExerciseWorkItems(const Subject& subject, const StudyGroup& group, const std::vector<WorkItem>& generated, std::vector<WorkItem>& result);
    static void GenerateSeminarWorkItems(const Subject& subject, const StudyGroup& group, const std::vector<WorkItem>& generated, std::vector<WorkItem>& result);
    static void GenerateCompletionWorkItems(const Subject& subject, const StudyGroup& group, const std::vector<WorkItem>& generated, std::vector<WorkItem>& result);
    static std::vector<int>::iterator HandleGroupCapacityChange(const WorkItemType type, const std::vector<WorkItem>& generated, std::vector<int>& groups, std::vector<WorkItem>& result);

    GeneratorLogic() = delete;
};

#endif // GENERATORLOGIC_H
