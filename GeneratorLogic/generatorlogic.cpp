#include "generatorlogic.h"
#include <math.h>
#include <QUuid>

std::vector<WorkItem> GeneratorLogic::Generate(
    const StudyGroup& group,
    const std::vector<Subject> &subjects,
    const std::vector<WorkItem> &generated
)
{
    std::vector<WorkItem> result;

    if (subjects.size() == 0)
        return result;

    for (const auto& subject: subjects)
    {
        GenerateLectureWorkItems(subject, group, generated, result);
        GenerateExerciseWorkItems(subject, group, generated, result);
        GenerateSeminarWorkItems(subject, group, generated, result);
        GenerateCompletionWorkItems(subject, group, generated, result);
    }

    return result;
}

QString GeneratorLogic::GenerateName(const WorkItem &wi, const Subject &sub)
{
    QString generatedName;
    generatedName.append("Gen_");
    generatedName.append(sub.m_abbrevation);
    generatedName.append("_");
    generatedName.append(EnumMapper::ToString(wi.m_language));
    generatedName.append("_");
    generatedName.append(EnumMapper::ToString(wi.m_type));
    generatedName.append("_");
    generatedName.append(QUuid::createUuid().toString(QUuid::WithoutBraces));

    return generatedName;
}

bool GeneratorLogic::ExistsItem(const std::vector<WorkItem> &generated, const WorkItemType t)
{
    return std::any_of(
        generated.begin(),
        generated.end(),
        [t](const auto w) { return w.m_type == t; }
    );
}

std::vector<WorkItem> GeneratorLogic::GetItems(const std::vector<WorkItem> &items, const WorkItemType t)
{
    std::vector<WorkItem> filtered(items.size());

    auto it = std::copy_if(
        items.begin(),
        items.end(),
        filtered.begin(),
        [t](const auto& i) { return i.m_type == t; }
    );

    filtered.resize(std::distance(filtered.begin(), it));

    return filtered;
}

std::vector<int> GeneratorLogic::SplitToGroups(const int enrolled, const int capacity)
{
    int groups = ::ceil(static_cast<double>(enrolled) / capacity);
    int single = ::ceil(enrolled / groups);

    std::vector<int> capacityOfGroups(groups - 1, single);
    capacityOfGroups.push_back(enrolled - (groups -1) * single);

    return capacityOfGroups;
}

void GeneratorLogic::GenerateLectureWorkItems(const Subject &subject, const StudyGroup &group, const std::vector<WorkItem> &generated, std::vector<WorkItem> &result)
{
    if (subject.m_lectureHours > 0)
    {
        auto wi = WorkItem();
        bool updating = false;

        if (ExistsItem(generated, WorkItemType::Lecture))
        {
            auto it = GetItems(generated, WorkItemType::Lecture);
            if (it.size() == 1)
            {
                wi = it[0];
                updating = true;
            }
        }

        wi.m_numberOfHours = subject.m_lectureHours;
        wi.m_numberOfWeeks = subject.m_weeks;
        wi.m_numberOfStudents = group.m_enrolled;
        wi.m_subjectId = subject.m_id;
        wi.m_language = subject.m_language;
        wi.m_type = WorkItemType::Lecture;

        if (!updating)
        {
            wi.m_name = GenerateName(wi, subject);
        }

        result.push_back(wi);
    }
}

void GeneratorLogic::GenerateExerciseWorkItems(const Subject &subject, const StudyGroup &group, const std::vector<WorkItem> &generated, std::vector<WorkItem> &result)
{
    if (subject.m_exerciseHours > 0)
    {
        auto groups = SplitToGroups(group.m_enrolled, subject.m_groupCapacity);

        auto groupIterator = HandleGroupCapacityChange(WorkItemType::Exercise, generated, groups, result);

        for(; groupIterator != groups.end(); ++groupIterator)
        {
            auto wi = WorkItem();
            wi.m_numberOfHours = subject.m_exerciseHours;
            wi.m_numberOfWeeks = subject.m_weeks;
            wi.m_numberOfStudents = *groupIterator;
            wi.m_subjectId = subject.m_id;
            wi.m_language = subject.m_language;
            wi.m_type = WorkItemType::Exercise;

            wi.m_name = GenerateName(wi, subject);

            result.push_back(wi);
        }
    }
}

void GeneratorLogic::GenerateSeminarWorkItems(const Subject &subject, const StudyGroup &group, const std::vector<WorkItem> &generated, std::vector<WorkItem> &result)
{
    if (subject.m_seminarHours > 0)
    {
        auto groups = SplitToGroups(group.m_enrolled, subject.m_groupCapacity);

        auto groupIterator = HandleGroupCapacityChange(WorkItemType::Seminar, generated, groups, result);

        for(; groupIterator != groups.end(); ++groupIterator)
        {
            auto wi = WorkItem();
            wi.m_numberOfHours = subject.m_seminarHours;
            wi.m_numberOfWeeks = subject.m_weeks;
            wi.m_numberOfStudents = *groupIterator;
            wi.m_subjectId = subject.m_id;
            wi.m_language = subject.m_language;
            wi.m_type = WorkItemType::Seminar;

            wi.m_name = GenerateName(wi, subject);

            result.push_back(wi);
        }
    }
}

void GeneratorLogic::GenerateCompletionWorkItems(const Subject &subject, const StudyGroup &group, const std::vector<WorkItem> &generated, std::vector<WorkItem> &result)
{
    auto wi = WorkItem();
    bool updating = false;

    const auto completionTypes = std::vector<WorkItemType>{
        WorkItemType::ClassifiedCredit,
        WorkItemType::Credit,
        WorkItemType::Exam
    };

    for (const auto& type: completionTypes)
    {
        if (ExistsItem(generated, type))
        {
            auto it = GetItems(generated, type);
            if (it.size() == 1)
            {
                wi = it[0];
                updating = true;
                break;
            }
        }
    }



    wi.m_numberOfHours = 0;
    wi.m_numberOfWeeks = 0;
    wi.m_numberOfStudents = group.m_enrolled;
    wi.m_subjectId = subject.m_id;
    wi.m_language = subject.m_language;
    wi.m_type = EnumMapper::MapToWorkItemType(subject.m_completion);

    if (!updating)
    {
        wi.m_name = GenerateName(wi, subject);
    }

    result.push_back(wi);
}

std::vector<int>::iterator GeneratorLogic::HandleGroupCapacityChange(const WorkItemType type, const std::vector<WorkItem> &generated, std::vector<int> &groups, std::vector<WorkItem> &result)
{
    auto groupIterator = groups.begin();

    if (ExistsItem(generated, type))
    {
        auto it = GetItems(generated, type);

        const auto generatedSize = it.size();
        const auto groupsSize = groups.size();

        // Adding new groups
        if (generatedSize < groupsSize)
        {
            for (auto& generated: it)
            {
                generated.m_numberOfStudents = *groupIterator;
                result.push_back(generated);
                ++groupIterator;
            }
        }
        else if (generatedSize > groupsSize)
        {
            const auto diff = generatedSize - groupsSize;
            for (auto i = 0u; i < diff; ++i)
            {
                groups.push_back(0);
            }
            groupIterator = groups.begin();
        }
        else
        {
            // Just confirm number of students
            for (auto& generated: it)
            {
                generated.m_numberOfStudents = *groupIterator;
                result.push_back(generated);
                ++groupIterator;
            }
        }
    }
    return groupIterator;
}
