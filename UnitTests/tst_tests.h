#ifndef TST_TESTS_H
#define TST_TESTS_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "validator.h"

using namespace testing;

TEST(ValidatorTests, SimpleValidatorAlwaysReturnsTrue)
{
    const auto validator = std::make_shared<Validator>();

    ASSERT_TRUE(validator->Validate("Something"));
    ASSERT_TRUE(validator->Validate(QString()));
}

TEST(ValidatorTests, RequiredValidator)
{
    const auto validator = std::make_shared<RequiredValidator>();

    ASSERT_TRUE(validator->Validate("Something"));
    ASSERT_FALSE(validator->Validate(QString()));
}

TEST(ValidatorTests, RegexValidator)
{
    const auto validator = std::make_shared<RegexValidator>(QRegularExpression("ab.df"));

    ASSERT_TRUE(validator->Validate("abcdf"));
    ASSERT_FALSE(validator->Validate(QString()));
}

TEST(ValidatorTests, ContainsValidatorCheckForSingleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@");

    ASSERT_TRUE(validator->Validate("ab@df"));
}

TEST(ValidatorTests, ContainsValidatorCheckForNoSingleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@");

    ASSERT_FALSE(validator->Validate("QStr"));
}

TEST(ValidatorTests, ContainsValidatorCheckForMultipleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@.");

    ASSERT_TRUE(validator->Validate("ab@d.f"));
}

TEST(ValidatorTests, ContainsValidatorCheckForNoMultipleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@[");

    ASSERT_FALSE(validator->Validate("QStr"));
    ASSERT_FALSE(validator->Validate("Q[Str"));
}

TEST(ValidatorTests, NotContainsValidatorCheckForSingleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@", ContainsValidator::Operation::NEGATIVE);

    ASSERT_TRUE(validator->Validate("abdf"));
}

TEST(ValidatorTests, NotContainsValidatorCheckForNoSingleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@", ContainsValidator::Operation::NEGATIVE);

    ASSERT_FALSE(validator->Validate("Q@Str"));
}

TEST(ValidatorTests, NotContainsValidatorCheckForMultipleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@.", ContainsValidator::Operation::NEGATIVE);

    ASSERT_TRUE(validator->Validate("abdf"));
}

TEST(ValidatorTests, NotContainsValidatorCheckForNoMultipleCharacter)
{
    const auto validator = std::make_shared<ContainsValidator>("@(", ContainsValidator::Operation::NEGATIVE);

    ASSERT_FALSE(validator->Validate("Q@St(r"));
}

TEST(ValidatorTests, ContainsNotRequired)
{
    auto builder = ComposedValidator::create()
        .contains("@");
    const auto validator =  std::make_shared<ComposedValidator>(builder);
    ASSERT_TRUE(validator->Validate(""));
}

TEST(ValidatorTests, NotRequiredEmailValidator)
{
    auto builder = ComposedValidator::create()
        .contains("@")
        .contains(".");
    const auto validator =  std::make_shared<ComposedValidator>(builder);
    ASSERT_TRUE(validator->Validate(""));
    ASSERT_TRUE(validator->Validate("fu@fa.cz"));
}

TEST(ComposedValidatorTests, ContainsRequired)
{
    auto builder = ComposedValidator::create()
            .contains("12")
            .required();


    const auto validator = std::make_shared<ComposedValidator>(builder);

    ASSERT_TRUE(validator->Validate("Q12r"));
}

#endif // TST_TESTS_H
