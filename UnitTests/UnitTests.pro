GOOGLETEST_DIR = "C:\google_test-1.10.0"
include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG += qt

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += core gui sql

HEADERS += \
        generatorTests.h \
        tst_tests.h

SOURCES += \
        main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Validation/release/ -lValidation
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Validation/debug/ -lValidation
else:unix: LIBS += -L$$OUT_PWD/../Validation/ -lValidation

INCLUDEPATH += $$PWD/../Validation
DEPENDPATH += $$PWD/../Validation

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GeneratorLogic/release/ -lGeneratorLogic
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GeneratorLogic/debug/ -lGeneratorLogic
else:unix: LIBS += -L$$OUT_PWD/../GeneratorLogic/ -lGeneratorLogic

INCLUDEPATH += $$PWD/../GeneratorLogic
DEPENDPATH += $$PWD/../GeneratorLogic

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Database/release/ -lDatabase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Database/debug/ -lDatabase
else:unix: LIBS += -L$$OUT_PWD/../Database/ -lDatabase

INCLUDEPATH += $$PWD/../Database
DEPENDPATH += $$PWD/../Database
