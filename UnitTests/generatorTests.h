#ifndef GENERATORTESTS_H
#define GENERATORTESTS_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "generatorlogic.h"
#include "Enums/WorkItemType.h"

using namespace testing;

using WorkItemList = std::vector<WorkItem>;

bool hasExpectedWorkItems(const std::vector<WorkItemType>& expected, const std::vector<WorkItem>& workItems)
{
    if (expected.size() != workItems.size())
        return false;

    std::vector<int> indexes(expected.size());
    std::iota(indexes.begin(), indexes.end(), 0);
    std::set<int> idx(indexes.begin(), indexes.end());

    for (const auto& wit: expected)
    {
        for (auto t = idx.begin(); t != idx.end(); ++t)
        {
            if (wit == workItems[*t].m_type)
            {
                idx.erase(t);
                break;
            }
        }
    }

    return idx.size() == 0;
}

const WorkItem& getWorkItem(const WorkItemList& items, WorkItemType type, int skip = 0)
{
    for (auto it = items.begin(); it != items.end(); ++it)
    {
        if (type == it->m_type)
        {
            if (skip)
            {
                --skip;
                continue;
            }
            return *it;
        }
    }
    throw std::invalid_argument("Item not found!");
}

TEST(WorkItemsGenerator, ShouldReturnEmptyVectorWhenStudyGroupHasNoSubjects)
{
    const auto group = StudyGroup(12);
    const WorkItemList result = GeneratorLogic::Generate(group, std::vector<Subject>());

    ASSERT_EQ(result.size(), 0);
}

TEST(WorkItemsGenerator, ShouldReturnTwoWorkItemsWhenSubjectHasOnlyLecture)
{
    auto group = StudyGroup(12);
    group.m_enrolled = 10;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_groupCapacity = group.m_enrolled;
    subject.m_completion = Completion::ClassifiedCredit;
    const auto subjects = std::vector<Subject>{subject};

    auto expected = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::ClassifiedCredit
    };

    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    ASSERT_EQ(result.size(), 2);
    ASSERT_FALSE(result[0].m_name.isEmpty());
    ASSERT_TRUE(hasExpectedWorkItems(expected, result));
}

TEST(WorkItemsGenerator, ShouldReturnTwoWorkItemWhenSubjectHasLectureAndExercise)
{
    auto group = StudyGroup(42);
    group.m_enrolled = 12;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_exerciseHours = 1;
    subject.m_completion = Completion::Exam;
    subject.m_groupCapacity = group.m_enrolled;
    const auto subjects = std::vector<Subject>{subject};


    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    auto expectedWorkItems = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Exercise,
        WorkItemType::Exam
    };

    ASSERT_EQ(result.size(), 3);

    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, result));
}

TEST(WorkItemsGenerator, ShouldReturnThreeWorkItemWhenSubjectHasLectureAndExercise)
{
    auto group = StudyGroup(42);
    group.m_enrolled = 20;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_exerciseHours = 1;
    subject.m_completion = Completion::Exam;
    subject.m_groupCapacity = 12;
    const auto subjects = std::vector<Subject>{subject};


    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    auto expectedWorkItems = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Exercise,
        WorkItemType::Exercise,
        WorkItemType::Exam
    };

    ASSERT_EQ(result.size(), 4);

    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, result));
}

TEST(WorkItemsGenerator, ShouldReturnUpdatedWorkItemsWhenGenerateIsCalledWithAlreadyExistingWorkItems)
{
    auto group = StudyGroup(12);
    group.m_enrolled = 10;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_groupCapacity = group.m_enrolled;
    subject.m_completion = Completion::ClassifiedCredit;
    const auto subjects = std::vector<Subject>{subject};

    auto expected = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::ClassifiedCredit
    };

    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    ASSERT_EQ(result.size(), 2);
    ASSERT_GT(getWorkItem(result, WorkItemType::Lecture).m_name.size(), 0);
    ASSERT_TRUE(hasExpectedWorkItems(expected, result));

    group.m_enrolled = 15;

    const WorkItemList secondResult = GeneratorLogic::Generate(group, subjects, result);

    ASSERT_EQ(secondResult.size(), 2);
    ASSERT_TRUE(hasExpectedWorkItems(expected, secondResult));
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Lecture).m_numberOfStudents, group.m_enrolled);
}


TEST(WorkItemsGenerator, ShouldCreateAnotherWorkItemWhenGroupCapacityIsIncreased)
{
    auto group = StudyGroup(42);
    group.m_enrolled = 12;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_exerciseHours = 1;
    subject.m_seminarHours = 0;
    subject.m_completion = Completion::Exam;
    subject.m_groupCapacity = group.m_enrolled;
    const auto subjects = std::vector<Subject>{subject};


    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    const auto expectedWorkItems = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Exercise,
        WorkItemType::Exam
    };

    ASSERT_EQ(result.size(), 3);

    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, result));
    group.m_enrolled = 21;
    const WorkItemList secondResult = GeneratorLogic::Generate(group, subjects, result);

    const auto expectedWorkItemsAfterIncreasingCapacity = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Exercise,
        WorkItemType::Exercise,
        WorkItemType::Exam
    };

    ASSERT_EQ(secondResult.size(), 4);
    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItemsAfterIncreasingCapacity, secondResult));
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Exercise).m_numberOfStudents, group.m_enrolled / 2);
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Exercise, 1).m_numberOfStudents, group.m_enrolled - (group.m_enrolled / 2));
}

TEST(WorkItemsGenerator, ShouldSetZeroStudentsInUnusedWorkItem)
{
    auto group = StudyGroup(42);
    group.m_enrolled = 21;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_exerciseHours = 1;
    subject.m_seminarHours = 0;
    subject.m_completion = Completion::Exam;
    subject.m_groupCapacity = 12;
    const auto subjects = std::vector<Subject>{subject};


    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    const auto expectedWorkItems = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Exercise,
        WorkItemType::Exercise,
        WorkItemType::Exam
    };

    ASSERT_EQ(result.size(), 4);

    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, result));
    group.m_enrolled = 12;
    const WorkItemList secondResult = GeneratorLogic::Generate(group, subjects, result);

    ASSERT_EQ(secondResult.size(), 4);
    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, secondResult));
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Exercise).m_numberOfStudents, group.m_enrolled);
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Exercise, 1).m_numberOfStudents, 0);
}

TEST(WorkItemsGenerator, ShouldCreateAnotherWorkItemWhenGroupCapacityIsIncreasedSeminar)
{
    auto group = StudyGroup(42);
    group.m_enrolled = 12;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_exerciseHours = 0;
    subject.m_seminarHours = 1;
    subject.m_completion = Completion::Exam;
    subject.m_groupCapacity = group.m_enrolled;
    const auto subjects = std::vector<Subject>{subject};


    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    const auto expectedWorkItems = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Seminar,
        WorkItemType::Exam
    };

    ASSERT_EQ(result.size(), 3);

    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, result));
    group.m_enrolled = 21;
    const WorkItemList secondResult = GeneratorLogic::Generate(group, subjects, result);

    const auto expectedWorkItemsAfterIncreasingCapacity = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Seminar,
        WorkItemType::Seminar,
        WorkItemType::Exam
    };

    ASSERT_EQ(secondResult.size(), 4);
    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItemsAfterIncreasingCapacity, secondResult));
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Seminar).m_numberOfStudents, group.m_enrolled / 2);
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Seminar, 1).m_numberOfStudents, group.m_enrolled - (group.m_enrolled / 2));
}

TEST(WorkItemsGenerator, ShouldSetZeroStudentsInUnusedWorkItemSeminar)
{
    auto group = StudyGroup(42);
    group.m_enrolled = 21;
    auto subject = Subject(1);
    subject.m_lectureHours = 1;
    subject.m_exerciseHours = 0;
    subject.m_seminarHours = 1;
    subject.m_completion = Completion::Exam;
    subject.m_groupCapacity = 12;
    const auto subjects = std::vector<Subject>{subject};


    const WorkItemList result = GeneratorLogic::Generate(group, subjects);

    const auto expectedWorkItems = std::vector<WorkItemType>{
        WorkItemType::Lecture,
        WorkItemType::Seminar,
        WorkItemType::Seminar,
        WorkItemType::Exam
    };

    ASSERT_EQ(result.size(), 4);

    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, result));
    group.m_enrolled = 12;
    const WorkItemList secondResult = GeneratorLogic::Generate(group, subjects, result);

    ASSERT_EQ(secondResult.size(), 4);
    ASSERT_TRUE(hasExpectedWorkItems(expectedWorkItems, secondResult));
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Seminar).m_numberOfStudents, group.m_enrolled);
    ASSERT_EQ(getWorkItem(secondResult, WorkItemType::Seminar, 1).m_numberOfStudents, 0);
}

#endif // GENERATORTESTS_H
