TEMPLATE = subdirs

SUBDIRS += \
    Database \
    Frontend \
    GeneratorLogic \
    UnitTests \
    Validation
