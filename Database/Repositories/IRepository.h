#ifndef IREPOSITORY_H
#define IREPOSITORY_H

#include <QtSql>

using Id = int;

class Transaction
{
protected:
    QSqlDatabase m_db;
public:
    explicit Transaction(QSqlDatabase db): m_db(db) { m_db.transaction(); };
    ~Transaction() { m_db.commit(); };
};

template <typename T>
class IRepository
{
public:
    IRepository(QSqlDatabase context);
    virtual QList<T> GetAll() = 0;
    virtual void Remove(const int id) = 0;
    virtual Id Add(const T* item) = 0;
    virtual bool Exists(const T* item) = 0;
    virtual int Count() = 0;
    virtual void Update(const T* item) = 0;
    Transaction GetTransaction();
protected:
    virtual void CreateTable() = 0;
    virtual void CreateDependentTables();
    void Execute(QSqlQuery& query, const bool execBatch = false);

    QVariant IdOrNull(const Id id);

    QSqlDatabase m_context;
};

template<typename T>
IRepository<T>::IRepository(QSqlDatabase context): m_context(context)
{
}

template<typename T>
Transaction IRepository<T>::GetTransaction()
{
    return Transaction(m_context);
}

template<typename T>
void IRepository<T>::CreateDependentTables()
{
}

template<typename T>
void IRepository<T>::Execute(QSqlQuery &query, const bool execBatch)
{
    assert(m_context.isOpen() == true);

    auto success = false;
    if (!execBatch)
    {
        success = query.exec();
    }
    else
    {
        success = query.execBatch();
    }
    qDebug() << query.executedQuery();

    if (!success)
    {
        qDebug() << query.executedQuery()
                 << " Failed: "
                 << query.lastError();
    }
}

template<typename T>
QVariant IRepository<T>::IdOrNull(const Id id)
{
    return id > 0
        ? QVariant(id)
        : QVariant();
}

#endif // IREPOSITORY_H
