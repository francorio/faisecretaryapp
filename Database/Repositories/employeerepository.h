#ifndef EMPLOYEEREPOSITORY_H
#define EMPLOYEEREPOSITORY_H

#include "Database_global.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSql>
#include "IRepository.h"
#include "../Models/employee.h"

class DATABASE_EXPORT EmployeeRepository: public IRepository<Employee>
{
public:
    EmployeeRepository(QSqlDatabase context);
    QList<Employee> GetAll() override;
    void Remove(const int id) override;
    Id Add(const Employee* employee) override;
    bool Exists(const Employee* employee) override;
    int Count() override;
    void Update(const Employee *item) override;

    Employee Get(const Id id);

protected:
    void CreateTable() override;
};

#endif // EMPLOYEEREPOSITORY_H
