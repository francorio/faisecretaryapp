#ifndef STUDYFORMREPOSITORY_H
#define STUDYFORMREPOSITORY_H

#include "Database_global.h"

#include "Utils/enummapper.h"

#include "IRepository.h"
#include "Models/studygroup.h"

class DATABASE_EXPORT StudyGroupRepository : public IRepository<StudyGroup>
{
public:
    StudyGroupRepository(QSqlDatabase context);
    QList<StudyGroup> GetAll() override;
    void Remove(const int id) override;
    Id Add(const StudyGroup* studyGroup) override;
    bool Exists(const StudyGroup* studyGroup) override;
    int Count() override;
    void Update(const StudyGroup* item) override;

    QList<Id> GetStudyGroupsForSubject(const Id subjectId);

    void DisconnectStudyGroupFromSubjects(const StudyGroup* studyGroup);

protected:
    void CreateTable() override;
};

#endif // STUDYFORMREPOSITORY_H
