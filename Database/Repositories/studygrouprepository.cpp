#include "studygrouprepository.h"

StudyGroupRepository::StudyGroupRepository(QSqlDatabase context)
    : IRepository<StudyGroup>(context)
{
    CreateTable();
}

QList<StudyGroup> StudyGroupRepository::GetAll()
{
    QSqlQuery selectAll(m_context);
    selectAll.prepare("SELECT * FROM StudyGroups");

    Execute(selectAll);

    const auto record = selectAll.record();

    const auto idIdx = record.indexOf("Id");
    const auto nameIdx = record.indexOf("Abbrevation");
    const auto studyFormIdx = record.indexOf("StudyForm");
    const auto enrolledIdx = record.indexOf("Enrolled");
    const auto semesterIdx = record.indexOf("Semester");
    const auto yearIdx = record.indexOf("Year");
    const auto languageIdx = record.indexOf("Language");

    auto result = QList<StudyGroup>();

    while (selectAll.next())
    {
        auto sg = StudyGroup();
        sg.m_id = selectAll.value(idIdx).toInt();
        sg.m_abbrevation = selectAll.value(nameIdx).toString();
        sg.m_studyForm = EnumMapper::ToStudyFormEnum(selectAll.value(studyFormIdx).toString());
        sg.m_enrolled = selectAll.value(enrolledIdx).toInt();
        sg.m_semester = EnumMapper::ToSemesterEnum(selectAll.value(semesterIdx).toString());
        sg.m_year = selectAll.value(yearIdx).toInt();
        sg.m_language = EnumMapper::ToLanguageEnum(selectAll.value(languageIdx).toString());

        result.append(sg);

    }
    return result;
}

void StudyGroupRepository::Remove(const int id)
{
    QSqlQuery deleteQuery(m_context);
    deleteQuery.prepare("DELETE FROM StudyGroups WHERE Id = :studyGroupId");
    deleteQuery.bindValue(":studyGroupId", id);

    Execute(deleteQuery);
}

Id StudyGroupRepository::Add(const StudyGroup *studyGroup)
{
    QSqlQuery addQuery(m_context);

    addQuery.prepare(
        "INSERT INTO StudyGroups ("
            "Abbrevation, "
            "StudyForm, "
            "Enrolled, "
            "Semester, "
            "Year, "
            "Language"
        ") VALUES ("
            ":abbrevation, "
            ":studyForm, "
            ":enrolled, "
            ":semester, "
            ":year, "
            ":language"
        ")"
    );
    addQuery.bindValue(":abbrevation", studyGroup->m_abbrevation);
    addQuery.bindValue(":studyForm", EnumMapper::ToString(studyGroup->m_studyForm));
    addQuery.bindValue(":enrolled", studyGroup->m_enrolled);
    addQuery.bindValue(":semester", EnumMapper::ToString(studyGroup->m_semester));
    addQuery.bindValue(":year", studyGroup->m_year);
    addQuery.bindValue(":language", EnumMapper::ToString(studyGroup->m_language));

    Execute(addQuery);

    return addQuery.lastInsertId().toInt();
}

bool StudyGroupRepository::Exists(const StudyGroup *studyGroup)
{
    QSqlQuery exists(m_context);
    exists.prepare(
        "SELECT count(*) CNT FROM StudyGroups "
        "WHERE "
            "Abbrevation = :abbrevation"
    );

    exists.bindValue(":abbrevation", studyGroup->m_abbrevation);

    Execute(exists);

    const auto success = exists.first();
    if (!success)
    {
        qDebug() << exists.lastError();
    }

    const auto record = exists.record();
    const auto idx = record.indexOf("CNT");
    const auto existing = record.value(idx).toInt();

    return existing > 0;

}

int StudyGroupRepository::Count()
{
    QSqlQuery count(m_context);
    count.prepare("SELECT count(*) CNT FROM StudyGroups");

    Execute(count);

    const auto success = count.first();
    if (!success)
    {
        qDebug() << count.lastError();
    }

    const auto record = count.record();
    const auto idx = record.indexOf("CNT");
    const auto existing = record.value(idx).toInt();

    return existing;
}

void StudyGroupRepository::Update(const StudyGroup *item)
{
    QSqlQuery update(m_context);

    update.prepare(
        "UPDATE StudyGroups SET "
            "Abbrevation = :abbrevation, "
            "StudyForm = :studyForm, "
            "Enrolled = :enrolled, "
            "Semester = :semester, "
            "Year = :year, "
            "Language = :language "
        "WHERE Id = :studyGroupId"
    );

    update.bindValue(":abbrevation", item->m_abbrevation);
    update.bindValue(":studyForm", EnumMapper::ToString(item->m_studyForm));
    update.bindValue(":enrolled", item->m_enrolled);
    update.bindValue(":semester", EnumMapper::ToString(item->m_semester));
    update.bindValue(":year", item->m_year);
    update.bindValue(":language", EnumMapper::ToString(item->m_language));
    update.bindValue(":studyGroupId", item->m_id);

    Execute(update);
}

QList<Id> StudyGroupRepository::GetStudyGroupsForSubject(const Id subjectId)
{
    QSqlQuery studyGroupForSubject(m_context);
    studyGroupForSubject.prepare(
        "SELECT sg.Id FROM "
            "StudyGroups sg "
            "JOIN Subjects_StudyGroups ss ON sg.Id = ss.StudyGroupId "
            "JOIN Subjects s ON ss.SubjectId = s.Id "
        "WHERE "
            "s.Id = :subId"
    );

    studyGroupForSubject.bindValue(":subId", subjectId);

    Execute(studyGroupForSubject);

    const auto record = studyGroupForSubject.record();

    const auto idIdx = record.indexOf("Id");

    auto result = QList<Id>();

    while (studyGroupForSubject.next())
    {
        result.append(studyGroupForSubject.value(idIdx).toInt());
    }

    return result;
}

void StudyGroupRepository::DisconnectStudyGroupFromSubjects(const StudyGroup *studyGroup)
{
    QSqlQuery deleteQuery(m_context);

    deleteQuery.prepare(
        "DELETE FROM Subjects_StudyGroups "
        "WHERE StudyGroupId = :studyGroupId"
    );

    deleteQuery.bindValue(":studyGroupId", studyGroup->m_id);

    Execute(deleteQuery);
}

void StudyGroupRepository::CreateTable()
{
    QSqlQuery createTableQuery(m_context);
    createTableQuery.prepare(
        "CREATE TABLE IF NOT EXISTS StudyGroups "
        "("
            "Id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "Abbrevation TEXT NOT NULL UNIQUE, "
            "StudyForm TEXT NOT NULL, "
            "Enrolled INT NOT NULL, "
            "Semester TEXT NOT NULL, "
            "Year INT NOT NULL, "
            "Language TEXT NOT NULL "
        ")"
    );
    Execute(createTableQuery);
}
