#ifndef WORKITEMREPOSITORY_H
#define WORKITEMREPOSITORY_H

#include "Database_global.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSql>
#include "IRepository.h"
#include "../Models/workitem.h"

#include "Utils/enummapper.h"

class DATABASE_EXPORT WorkItemRepository: public IRepository<WorkItem>
{
public:
    WorkItemRepository(QSqlDatabase context);

    QList<WorkItem> GetAll() override;
    void Remove(const int id) override;
    Id Add(const WorkItem* workItem) override;
    bool Exists(const WorkItem* workItem) override;
    int Count() override;
    void Update(const WorkItem* item) override;

    QList<Id> GetWorkItemsForEmployee(const Id employeeId);
    QList<WorkItem> GetCompleteWorkItemsForEmployee(const Id employeeId);
    QList<WorkItem> GetWorkItemsForSubjects(const std::list<Id> subjectIds);
    void UpdateWorkItems(const Id employeeId, std::list<Id> workItems);

private:
    void CreateTable() override;
};

#endif // WORKITEMREPOSITORY_H
