#include "workitemrepository.h"

WorkItemRepository::WorkItemRepository(QSqlDatabase context)
    :IRepository<WorkItem>(context)
{
    CreateTable();
}

QList<WorkItem> WorkItemRepository::GetAll()
{
    QSqlQuery selectAll(m_context);
    selectAll.prepare("SELECT * FROM WorkItems");

    Execute(selectAll);

    const auto record = selectAll.record();

    const auto idIdx = record.indexOf("Id");
    const auto nameIdx = record.indexOf("Name");
    const auto workItemTypeIdx = record.indexOf("WorkItemType");
    const auto employeeIdIdx = record.indexOf("EmployeeId");
    const auto SubjectIdIdx = record.indexOf("SubjectId");
    const auto numberOfStudentsIdx = record.indexOf("NumberOfStudents");
    const auto numberOfHoursIdx = record.indexOf("NumberOfHours");
    const auto numberOfWeeksIdx = record.indexOf("NumberOfWeeks");
    const auto languageIdx = record.indexOf("Language");

    auto result = QList<WorkItem>();

    while (selectAll.next())
    {
        auto sg = WorkItem();
        sg.m_id = selectAll.value(idIdx).toInt();
        sg.m_name = selectAll.value(nameIdx).toString();
        sg.m_type = EnumMapper::ToWorkItemTypeEnum(selectAll.value(workItemTypeIdx).toString());
        sg.m_employeeId = selectAll.value(employeeIdIdx).toInt();
        sg.m_subjectId = selectAll.value(SubjectIdIdx).toInt();
        sg.m_numberOfStudents = selectAll.value(numberOfStudentsIdx).toInt();
        sg.m_numberOfHours = selectAll.value(numberOfHoursIdx).toInt();
        sg.m_numberOfWeeks = selectAll.value(numberOfWeeksIdx).toInt();
        sg.m_language = EnumMapper::ToLanguageEnum(selectAll.value(languageIdx).toString());
        result.append(sg);

    }
    return result;
}

void WorkItemRepository::Remove(const int id)
{
    QSqlQuery deleteQuery(m_context);
    deleteQuery.prepare("DELETE FROM WorkItems WHERE Id = :workItemId");
    deleteQuery.bindValue(":workItemId", id);

    Execute(deleteQuery);
}

Id WorkItemRepository::Add(const WorkItem *workItem)
{
    QSqlQuery addQuery(m_context);

    addQuery.prepare(
        "INSERT INTO WorkItems ("
        "Name, "
        "WorkItemType, "
        "EmployeeId, "
        "SubjectId, "
        "NumberOfStudents, "
        "NumberOfHours, "
        "NumberOfWeeks, "
        "Language"
        ") VALUES ("
            ":name, "
            ":workItemType, "
            ":employeeId, "
            ":subjectId, "
            ":numberOfStudents, "
            ":numberOfHours, "
            ":numberOfWeeks, "
            ":language"
        ")"
    );
    addQuery.bindValue(":name", workItem->m_name);
    addQuery.bindValue(":workItemType", EnumMapper::ToString(workItem->m_type));
    addQuery.bindValue(":employeeId", IdOrNull(workItem->m_employeeId));
    addQuery.bindValue(":subjectId", IdOrNull(workItem->m_subjectId));
    addQuery.bindValue(":numberOfStudents", workItem->m_numberOfStudents);
    addQuery.bindValue(":numberOfHours", workItem->m_numberOfHours);
    addQuery.bindValue(":numberOfWeeks", workItem->m_numberOfWeeks);
    addQuery.bindValue(":language", EnumMapper::ToString(workItem->m_language));
    Execute(addQuery);

    return addQuery.lastInsertId().toInt();
}

bool WorkItemRepository::Exists(const WorkItem *workItem)
{
    QSqlQuery exists(m_context);
    exists.prepare(
        "SELECT count(*) CNT FROM WorkItems "
        "WHERE "
            "Name = :name"
    );

    exists.bindValue(":name", workItem->m_name);

    Execute(exists);

    const auto success = exists.first();
    if (!success)
    {
        qDebug() << exists.lastError();
    }

    const auto record = exists.record();
    const auto idx = record.indexOf("CNT");
    const auto existing = record.value(idx).toInt();

    return existing > 0;
}

int WorkItemRepository::Count()
{
    QSqlQuery getCount(m_context);
    getCount.prepare(
        "SELECT count(*) CNT FROM WorkItems "
    );

    Execute(getCount);

    const auto success = getCount.first();
    if (!success)
    {
        qDebug() << getCount.lastError();
    }

    const auto record = getCount.record();
    const auto idx = record.indexOf("CNT");
    const auto count = record.value(idx).toInt();

    return count;
}

void WorkItemRepository::Update(const WorkItem *item)
{
    QSqlQuery update(m_context);

    update.prepare(
        "UPDATE WorkItems SET "
            "Name = :name, "
            "WorkItemType = :workItemType, "
            "EmployeeId = :employeeId, "
            "SubjectId = :subjectId, "
            "NumberOfStudents = :numberOfStudents, "
            "NumberOfHours = :numberOfHours, "
            "NumberOfWeeks = :numberOfWeeks, "
            "Language = :language "
        "WHERE Id = :workItemId"
    );

    update.bindValue(":name", item->m_name);
    update.bindValue(":workItemType", EnumMapper::ToString(item ->m_type));
    update.bindValue(":employeeId", IdOrNull(item ->m_employeeId));
    update.bindValue(":subjectId", IdOrNull(item ->m_subjectId));
    update.bindValue(":numberOfStudents", item ->m_numberOfStudents);
    update.bindValue(":numberOfHours", item ->m_numberOfHours);
    update.bindValue(":numberOfWeeks", item ->m_numberOfWeeks);
    update.bindValue(":language", EnumMapper::ToString(item ->m_language));
    update.bindValue(":workItemId", item->m_id);

    Execute(update);
}

QList<Id> WorkItemRepository::GetWorkItemsForEmployee(const Id employeeId)
{
    QSqlQuery workItemsForEmployee(m_context);
    workItemsForEmployee.prepare(
        "SELECT wi.Id FROM WorkItems wi "
        "WHERE "
            "wi.EmployeeId = :employeeId"
    );

    workItemsForEmployee.bindValue(":employeeId", employeeId);

    Execute(workItemsForEmployee);

    const auto record = workItemsForEmployee.record();
    const auto idIdx = record.indexOf("Id");
    auto result = QList<Id>();

    while (workItemsForEmployee.next())
    {
        result.append(workItemsForEmployee.value(idIdx).toInt());
    }

    return result;
}

QList<WorkItem> WorkItemRepository::GetCompleteWorkItemsForEmployee(const Id employeeId)
{
    QSqlQuery selectForEmployee(m_context);
    selectForEmployee.prepare("SELECT * FROM WorkItems wi WHERE wi.EmployeeId = :employeeId");

    selectForEmployee.bindValue(":employeeId", employeeId);

    Execute(selectForEmployee);

    const auto record = selectForEmployee.record();

    const auto idIdx = record.indexOf("Id");
    const auto nameIdx = record.indexOf("Name");
    const auto workItemTypeIdx = record.indexOf("WorkItemType");
    const auto employeeIdIdx = record.indexOf("EmployeeId");
    const auto SubjectIdIdx = record.indexOf("SubjectId");
    const auto numberOfStudentsIdx = record.indexOf("NumberOfStudents");
    const auto numberOfHoursIdx = record.indexOf("NumberOfHours");
    const auto numberOfWeeksIdx = record.indexOf("NumberOfWeeks");
    const auto languageIdx = record.indexOf("Language");

    auto result = QList<WorkItem>();

    while (selectForEmployee.next())
    {
        auto sg = WorkItem();
        sg.m_id = selectForEmployee.value(idIdx).toInt();
        sg.m_name = selectForEmployee.value(nameIdx).toString();
        sg.m_type = EnumMapper::ToWorkItemTypeEnum(selectForEmployee.value(workItemTypeIdx).toString());
        sg.m_employeeId = selectForEmployee.value(employeeIdIdx).toInt();
        sg.m_subjectId = selectForEmployee.value(SubjectIdIdx).toInt();
        sg.m_numberOfStudents = selectForEmployee.value(numberOfStudentsIdx).toInt();
        sg.m_numberOfHours = selectForEmployee.value(numberOfHoursIdx).toInt();
        sg.m_numberOfWeeks = selectForEmployee.value(numberOfWeeksIdx).toInt();
        sg.m_language = EnumMapper::ToLanguageEnum(selectForEmployee.value(languageIdx).toString());
        result.append(sg);

    }
    return result;
}

QList<WorkItem> WorkItemRepository::GetWorkItemsForSubjects(const std::list<Id> subjectIds)
{
    QSqlQuery workItemsForSubjects(m_context);
    workItemsForSubjects.prepare(
        "SELECT * FROM WorkItems wi "
        "WHERE "
            "wi.SubjectId IN (:subjectIds)"
    );

    QStringList ids;
    std::for_each(subjectIds.begin(), subjectIds.end(), [&ids](const auto& id) { ids << QString::number(id); });
    QString idList = ids.join(", ");

    workItemsForSubjects.bindValue(":subjectIds", idList);

    Execute(workItemsForSubjects);

    const auto record = workItemsForSubjects.record();

    const auto idIdx = record.indexOf("Id");
    const auto nameIdx = record.indexOf("Name");
    const auto workItemTypeIdx = record.indexOf("WorkItemType");
    const auto employeeIdIdx = record.indexOf("EmployeeId");
    const auto SubjectIdIdx = record.indexOf("SubjectId");
    const auto numberOfStudentsIdx = record.indexOf("NumberOfStudents");
    const auto numberOfHoursIdx = record.indexOf("NumberOfHours");
    const auto numberOfWeeksIdx = record.indexOf("NumberOfWeeks");
    const auto languageIdx = record.indexOf("Language");

    auto result = QList<WorkItem>();

    while (workItemsForSubjects.next())
    {
        auto sg = WorkItem();
        sg.m_id = workItemsForSubjects.value(idIdx).toInt();
        sg.m_name = workItemsForSubjects.value(nameIdx).toString();
        sg.m_type = EnumMapper::ToWorkItemTypeEnum(workItemsForSubjects.value(workItemTypeIdx).toString());
        sg.m_employeeId = workItemsForSubjects.value(employeeIdIdx).toInt();
        sg.m_subjectId = workItemsForSubjects.value(SubjectIdIdx).toInt();
        sg.m_numberOfStudents = workItemsForSubjects.value(numberOfStudentsIdx).toInt();
        sg.m_numberOfHours = workItemsForSubjects.value(numberOfHoursIdx).toInt();
        sg.m_numberOfWeeks = workItemsForSubjects.value(numberOfWeeksIdx).toInt();
        sg.m_language = EnumMapper::ToLanguageEnum(workItemsForSubjects.value(languageIdx).toString());
        result.append(sg);
    }
    return result;

}

void WorkItemRepository::UpdateWorkItems(const Id employeeId, std::list<Id> workItems)
{
    QSqlQuery update(m_context);

    update.prepare(
        "UPDATE WorkItems SET "
            "EmployeeId = :employeeId "
        "WHERE Id = :workItemId"
    );

    QVariantList employeeIdList;
    for (auto i = 0u; i < workItems.size(); ++i)
        employeeIdList << employeeId;

    QVariantList workItemsIdList;

    std::for_each(
        workItems.begin(),
        workItems.end(),
        [&workItemsIdList](const Id id) { workItemsIdList << id; }
    );

    update.bindValue(":employeeId", employeeIdList);
    update.bindValue(":workItemId", workItemsIdList);

    const auto executeInBatchMode = true;

    Execute(update, executeInBatchMode);
}

void WorkItemRepository::CreateTable()
{
    QSqlQuery createTableQuery(m_context);
    createTableQuery.prepare(
        "CREATE TABLE IF NOT EXISTS WorkItems "
        "("
            "Id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "Name TEXT NOT NULL UNIQUE, "
            "WorkItemType TEXT NOT NULL, "
            "EmployeeId INTEGER, "
            "SubjectId INTEGER, "
            "NumberOfStudents INT NOT NULL, "
            "NumberOfHours INT NOT NULL, "
            "NumberOfWeeks INT NOT NULL, "
            "Language TEXT NOT NULL, "
            "isValid INT, "
        "FOREIGN KEY(EmployeeId) REFERENCES Employees(Id) ON DELETE SET NULL, "
        "FOREIGN KEY(SubjectId) REFERENCES Subjects(Id) ON DELETE SET NULL"
        ")"
    );
    Execute(createTableQuery);
}
