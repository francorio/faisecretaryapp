#include "employeerepository.h"

EmployeeRepository::EmployeeRepository(QSqlDatabase context)
    : IRepository<Employee>(context)
{
    CreateTable();
}

QList<Employee> EmployeeRepository::GetAll()
{
    QSqlQuery selectAll(m_context);
    selectAll.prepare("SELECT * FROM Employees");

    Execute(selectAll);

    const auto record = selectAll.record();

    const auto idIdx = record.indexOf("Id");
    const auto nameIdx = record.indexOf("Name");
    const auto surnameIdx = record.indexOf("Surname");
    const auto workEmailIdx = record.indexOf("WorkEmail");
    const auto personalEmailIdx = record.indexOf("PersonalEmail");
    const auto workPhoneIdx = record.indexOf("WorkPhone");
    const auto personalPhoneIdx = record.indexOf("PersonalPhone");
    const auto czWorkPointsIdx = record.indexOf("CZWorkPoints");
    const auto enWorkPointsIdx = record.indexOf("ENWorkPoints");
    const auto isInStudyIdx = record.indexOf("IsInStudy");
    const auto workLoadIdx = record.indexOf("WorkLoad");

    auto result = QList<Employee>();

    while (selectAll.next())
    {
        auto emp = Employee();
        emp.m_id = selectAll.value(idIdx).toInt();
        emp.m_name = selectAll.value(nameIdx).toString();
        emp.m_surname = selectAll.value(surnameIdx).toString();
        emp.m_workEmail = selectAll.value(workEmailIdx).toString();
        emp.m_personalEmail = selectAll.value(personalEmailIdx).toString();
        emp.m_workPhone = selectAll.value(workPhoneIdx).toString();
        emp.m_personalPhone = selectAll.value(personalPhoneIdx).toString();
        emp.m_CZWorkPoints = selectAll.value(czWorkPointsIdx).toInt();
        emp.m_ENWorkPoints = selectAll.value(enWorkPointsIdx).toInt();
        emp.m_isInStudy = selectAll.value(isInStudyIdx).toInt();
        emp.m_workLoad = selectAll.value(workLoadIdx).toDouble();

        result.append(emp);

    }
    return result;
}

void EmployeeRepository::Remove(const int id)
{
    QSqlQuery deleteQuery(m_context);
    deleteQuery.prepare("DELETE FROM Employees WHERE Id = :employeeId");
    deleteQuery.bindValue(":employeeId", id);

    Execute(deleteQuery);
}

Id EmployeeRepository::Add(const Employee* employee)
{
    QSqlQuery addQuery(m_context);

    addQuery.prepare(
        "INSERT INTO Employees ("
            "Name, "
            "Surname, "
            "WorkEmail, "
            "PersonalEmail, "
            "WorkPhone, "
            "PersonalPhone, "
            "CZWorkPoints, "
            "ENWorkPoints, "
            "IsInStudy, "
            "WorkLoad"
        ") VALUES ("
            ":name, "
            ":surname, "
            ":workEmail, "
            ":personalEmail, "
            ":workPhone, "
            ":personalPhone, "
            ":CZWorkPoints, "
            ":ENWorkPoints, "
            ":isInStudy, "
            ":workLoad"
        ")"
    );
    addQuery.bindValue(":name", employee->m_name);
    addQuery.bindValue(":surname", employee->m_surname);
    addQuery.bindValue(":workEmail", employee->m_workEmail);
    addQuery.bindValue(":personalEmail", employee->m_personalEmail);
    addQuery.bindValue(":workPhone", employee->m_workPhone);
    addQuery.bindValue(":personalPhone", employee->m_personalPhone);
    addQuery.bindValue(":CZWorkPoints", employee->m_CZWorkPoints);
    addQuery.bindValue(":ENWorkPoints", employee->m_ENWorkPoints);
    addQuery.bindValue(":isInStudy", employee->m_isInStudy);
    addQuery.bindValue(":workLoad", employee->m_workLoad);

    Execute(addQuery);

    return addQuery.lastInsertId().toInt();
}

bool EmployeeRepository::Exists(const Employee* employee)
{
    QSqlQuery exists(m_context);
    exists.prepare(
        "SELECT count(*) CNT FROM Employees "
        "WHERE "
            "Name = :name "
        "AND "
            "Surname = :surname "
        "AND "
            "WorkEmail = :workEmail"
    );

    exists.bindValue(":name", employee->m_name);
    exists.bindValue(":surname", employee->m_surname);
    exists.bindValue(":workEmail", employee->m_workEmail);

    Execute(exists);

    const auto success = exists.first();
    if (!success)
    {
        qDebug() << exists.lastError();
    }

    const auto record = exists.record();
    const auto idx = record.indexOf("CNT");
    const auto existing = record.value(idx).toInt();

    return existing > 0;
}

int EmployeeRepository::Count()
{
    QSqlQuery getCount(m_context);
    getCount.prepare(
        "SELECT count(*) CNT FROM Employees "
    );

    Execute(getCount);

    const auto success = getCount.first();
    if (!success)
    {
        qDebug() << getCount.lastError();
    }

    const auto record = getCount.record();
    const auto idx = record.indexOf("CNT");
    const auto count = record.value(idx).toInt();

    return count;
}

void EmployeeRepository::Update(const Employee *item)
{
    QSqlQuery update(m_context);

    update.prepare(
        "UPDATE Employees SET "
            "Name = :name, "
            "Surname = :surname, "
            "WorkEmail = :workEmail, "
            "PersonalEmail = :personalEmail, "
            "WorkPhone = :workPhone, "
            "PersonalPhone = :personalPhone, "
            "CZWorkPoints = :CZWorkPoints, "
            "ENWorkPoints = :ENWorkPoints, "
            "IsInStudy = :isInStudy, "
            "WorkLoad = :workLoad "
        "WHERE Id = :employeeId"
    );

    update.bindValue(":name", item->m_name);
    update.bindValue(":surname", item->m_surname);
    update.bindValue(":workEmail", item->m_workEmail);
    update.bindValue(":personalEmail", item->m_personalEmail);
    update.bindValue(":workPhone", item->m_workPhone);
    update.bindValue(":personalPhone", item->m_personalPhone);
    update.bindValue(":CZWorkPoints", item->m_CZWorkPoints);
    update.bindValue(":ENWorkPoints", item->m_ENWorkPoints);
    update.bindValue(":isInStudy", item->m_isInStudy);
    update.bindValue(":workLoad", item->m_workLoad);
    update.bindValue(":employeeId", item->m_id);

    Execute(update);
}

Employee EmployeeRepository::Get(const Id id)
{
    QSqlQuery selectOne(m_context);
    selectOne.prepare("SELECT * FROM Employees e WHERE e.Id = :id");

    selectOne.bindValue(":id", id);

    Execute(selectOne);

    const auto success = selectOne.first();
    if (!success)
    {
        qDebug() << selectOne.lastError();
    }

    auto record = selectOne.record();

    const auto idIdx = record.indexOf("Id");
    const auto nameIdx = record.indexOf("Name");
    const auto surnameIdx = record.indexOf("Surname");
    const auto workEmailIdx = record.indexOf("WorkEmail");
    const auto personalEmailIdx = record.indexOf("PersonalEmail");
    const auto workPhoneIdx = record.indexOf("WorkPhone");
    const auto personalPhoneIdx = record.indexOf("PersonalPhone");
    const auto czWorkPointsIdx = record.indexOf("CZWorkPoints");
    const auto enWorkPointsIdx = record.indexOf("ENWorkPoints");
    const auto isInStudyIdx = record.indexOf("IsInStudy");
    const auto workLoadIdx = record.indexOf("WorkLoad");


    auto emp = Employee();
    emp.m_id = record.value(idIdx).toInt();
    emp.m_name = record.value(nameIdx).toString();
    emp.m_surname = record.value(surnameIdx).toString();
    emp.m_workEmail = record.value(workEmailIdx).toString();
    emp.m_personalEmail = record.value(personalEmailIdx).toString();
    emp.m_workPhone = record.value(workPhoneIdx).toString();
    emp.m_personalPhone = record.value(personalPhoneIdx).toString();
    emp.m_CZWorkPoints = record.value(czWorkPointsIdx).toInt();
    emp.m_ENWorkPoints = record.value(enWorkPointsIdx).toInt();
    emp.m_isInStudy = record.value(isInStudyIdx).toInt();
    emp.m_workLoad = record.value(workLoadIdx).toDouble();

    return emp;
}


void EmployeeRepository::CreateTable()
{
    QSqlQuery createTableQuery(m_context);
    createTableQuery.prepare(
        "CREATE TABLE IF NOT EXISTS Employees "
        "("
            "Id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "Name TEXT NOT NULL, "
            "Surname TEXT NOT NULL, "
            "WorkEmail TEXT NOT NULL, "
            "PersonalEmail TEXT, "
            "WorkPhone TEXT NOT NULL, "
            "PersonalPhone TEXT, "
            "CZWorkPoints INTEGER, "
            "ENWorkPoints INTEGER, "
            "IsInStudy INTEGER, "
            "WorkLoad FLOAT"
        ")"
    );
    Execute(createTableQuery);
}
