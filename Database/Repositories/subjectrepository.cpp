#include "subjectrepository.h"

SubjectRepository::SubjectRepository(QSqlDatabase context)
    : IRepository<Subject>(context)
{
    CreateTable();
    CreateDependentTables();
}

QList<Subject> SubjectRepository::GetAll()
{
    QSqlQuery selectAll(m_context);
    selectAll.prepare("SELECT * FROM Subjects");

    Execute(selectAll);

    const auto record = selectAll.record();

    const auto idIdx = record.indexOf("Id");
    const auto abbrevationIdx = record.indexOf("Abbrevation");
    const auto titleIdx = record.indexOf("Title");
    const auto weeksIdx = record.indexOf("Weeks");
    const auto lectureHoursIdx = record.indexOf("LectureHours");
    const auto exerciseHoursIdx = record.indexOf("ExerciseHours");
    const auto seminarHoursIdx = record.indexOf("SeminarHours");
    const auto languageIdx = record.indexOf("Language");
    const auto groupCapacityIdx = record.indexOf("GroupCapacity");

    auto result = QList<Subject>();

    while (selectAll.next())
    {
        auto sg = Subject();
        sg.m_id = selectAll.value(idIdx).toInt();
        sg.m_abbrevation = selectAll.value(abbrevationIdx).toString();
        sg.m_title = selectAll.value(titleIdx).toString();
        sg.m_weeks = selectAll.value(weeksIdx).toInt();
        sg.m_lectureHours = selectAll.value(lectureHoursIdx).toInt();
        sg.m_exerciseHours = selectAll.value(exerciseHoursIdx).toInt();
        sg.m_seminarHours = selectAll.value(seminarHoursIdx).toInt();
        sg.m_language = EnumMapper::ToLanguageEnum(selectAll.value(languageIdx).toString());
        sg.m_groupCapacity = selectAll.value(groupCapacityIdx).toInt();
        result.append(sg);
    }
    return result;
}

void SubjectRepository::Remove(const int id)
{
    QSqlQuery deleteQuery(m_context);
    deleteQuery.prepare("DELETE FROM Subjects WHERE Id = :employeeId");
    deleteQuery.bindValue(":employeeId", id);

    Execute(deleteQuery);
}

Id SubjectRepository::Add(const Subject *subject)
{
    QSqlQuery addQuery(m_context);

    addQuery.prepare(
        "INSERT INTO Subjects ("
            "Abbrevation, "
            "Title, "
            "Completion, "
            "Weeks, "
            "LectureHours, "
            "ExerciseHours, "
            "SeminarHours, "
            "Language, "
            "GroupCapacity"
        ") VALUES ("
            ":abbrevation, "
            ":title, "
            ":completion, "
            ":weeks, "
            ":lectureHours, "
            ":exerciseHours, "
            ":seminarHours, "
            ":language, "
            ":groupCapacity"
        ")"
    );
    addQuery.bindValue(":abbrevation", subject->m_abbrevation);
    addQuery.bindValue(":title", subject->m_title);
    addQuery.bindValue(":completion", EnumMapper::ToString(subject->m_completion));
    addQuery.bindValue(":weeks", subject->m_weeks);
    addQuery.bindValue(":lectureHours", subject->m_lectureHours);
    addQuery.bindValue(":exerciseHours", subject->m_exerciseHours);
    addQuery.bindValue(":seminarHours", subject->m_seminarHours);
    addQuery.bindValue(":language", EnumMapper::ToString(subject->m_language));
    addQuery.bindValue(":groupCapacity", subject->m_groupCapacity);

    Execute(addQuery);

    return addQuery.lastInsertId().toInt();
}

bool SubjectRepository::Exists(const Subject *subject)
{
    QSqlQuery exists(m_context);
    exists.prepare(
        "SELECT count(*) CNT FROM Subjects "
        "WHERE "
            "Abbrevation = :abbrevation"
    );

    exists.bindValue(":abbrevation", subject->m_abbrevation);

    Execute(exists);

    const auto success = exists.first();
    if (!success)
    {
        qDebug() << exists.lastError();
    }

    const auto record = exists.record();
    const auto idx = record.indexOf("CNT");
    const auto existing = record.value(idx).toInt();

    return existing > 0;
}

void SubjectRepository::CreateTable()
{
    QSqlQuery createTableQuery(m_context);
    createTableQuery.prepare(
        "CREATE TABLE IF NOT EXISTS Subjects "
        "("
            "Id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "Abbrevation TEXT NOT NULL UNIQUE, "
            "Title TEXT, "
            "Completion TEXT NOT NULL, "
            "Weeks INTEGER DEFAULT 14, "
            "LectureHours INTEGER, "
            "ExerciseHours INTEGER, "
            "SeminarHours INTEGER, "
            "Language TEXT, "
            "GroupCapacity INTEGER"
        ")"
    );
    Execute(createTableQuery);
}

void SubjectRepository::CreateDependentTables()
{
    QSqlQuery createDependentTableQuery(m_context);
    createDependentTableQuery.prepare(
        "CREATE TABLE IF NOT EXISTS Subjects_StudyGroups "
        "("
            "SubjectId INTEGER, "
            "StudyGroupId INTEGER, "
            "FOREIGN KEY(SubjectId) REFERENCES Subjects(Id) ON DELETE CASCADE, "
            "FOREIGN KEY(StudyGroupId) REFERENCES StudyGroups(Id) ON DELETE CASCADE"
        ")"
    );
    Execute(createDependentTableQuery);
}

int SubjectRepository::Count()
{
    QSqlQuery getCount(m_context);
    getCount.prepare(
        "SELECT count(*) CNT FROM Subjects "
    );

    Execute(getCount);

    const auto success = getCount.first();
    if (!success)
    {
        qDebug() << getCount.lastError();
    }

    const auto record = getCount.record();
    const auto idx = record.indexOf("CNT");
    const auto count = record.value(idx).toInt();

    return count;
}

void SubjectRepository::ConnectSubjectsToStudyGroups(const std::list<Id> &subjectIds, const std::list<Id> &studyGroupIds)
{
    assert(subjectIds.size() == studyGroupIds.size());
    QSqlQuery addSubjectToStudyGroup(m_context);

    addSubjectToStudyGroup.prepare(
        "INSERT INTO Subjects_StudyGroups ("
            "SubjectId, "
            "StudyGroupId"
        ") VALUES ("
            ":subjectId, "
            ":studyGroupId"
        ")"
    );

    QVariantList subjectIdsList;
    QVariantList studyGroupIdsList;

    std::for_each(
        studyGroupIds.begin(),
        studyGroupIds.end(),
        [&studyGroupIdsList](const Id id) { studyGroupIdsList << id; });

    std::for_each(
        subjectIds.begin(),
        subjectIds.end(),
        [&subjectIdsList](const Id id) { subjectIdsList << id; });




    addSubjectToStudyGroup.bindValue(":subjectId", subjectIdsList);
    addSubjectToStudyGroup.bindValue(":studyGroupId", studyGroupIdsList);

    const auto executeInBatchMode = true;

    Execute(addSubjectToStudyGroup, executeInBatchMode);
}

void SubjectRepository::ConnectSubjectsToStudyGroups(const std::list<Id> subjectIds, const Id studyGroupId)
{
    auto sameStudyGroupIds = std::list<Id>();

    for (auto i = 0u; i < subjectIds.size(); ++i)
        sameStudyGroupIds.push_back(studyGroupId);

    ConnectSubjectsToStudyGroups(subjectIds, sameStudyGroupIds);
}

void SubjectRepository::ConnectSubjectsToStudyGroups(const int subjectId, const std::list<Id>& studyGroupIds)
{
    auto sameSubjectIds = std::list<Id>();

    for (auto i = 0u; i < studyGroupIds.size(); ++i)
        sameSubjectIds.push_back(subjectId);

    ConnectSubjectsToStudyGroups(sameSubjectIds, studyGroupIds);
}

void SubjectRepository::Update(const Subject *subject)
{
    QSqlQuery update(m_context);

    update.prepare(
        "UPDATE Subjects SET "
            "Abbrevation = :abbrevation, "
            "Title = :title, "
            "Completion = :completion, "
            "Weeks = :weeks, "
            "LectureHours = :lectureHours, "
            "ExerciseHours = :exerciseHours, "
            "SeminarHours = :seminarHours, "
            "Language = :language, "
            "GroupCapacity = :groupCapacity "
        "WHERE Id = :subjectId"
    );

    update.bindValue(":abbrevation", subject->m_abbrevation);
    update.bindValue(":title", subject->m_title);
    update.bindValue(":completion", EnumMapper::ToString(subject->m_completion));
    update.bindValue(":weeks", subject->m_weeks);
    update.bindValue(":lectureHours", subject->m_lectureHours);
    update.bindValue(":exerciseHours", subject->m_exerciseHours);
    update.bindValue(":seminarHours", subject->m_seminarHours);
    update.bindValue(":language", EnumMapper::ToString(subject->m_language));
    update.bindValue(":groupCapacity", subject->m_groupCapacity);
    update.bindValue(":subjectId", subject->m_id);

    Execute(update);
}

void SubjectRepository::DisconnectSubjectFromStudyGroups(const Subject *subject)
{
    QSqlQuery deleteQuery(m_context);

    deleteQuery.prepare(
        "DELETE FROM Subjects_StudyGroups "
        "WHERE SubjectId = :subjectId"
    );

    deleteQuery.bindValue(":subjectId", subject->m_id);

    Execute(deleteQuery);
}

QList<Id> SubjectRepository::GetSubjectsForStudyGroup(const Id studyGroupId)
{
    QSqlQuery subjectForStudyGroup(m_context);
    subjectForStudyGroup.prepare(
        "SELECT s.Id FROM "
            "Subjects s "
            "JOIN Subjects_StudyGroups ss ON s.Id = ss.SubjectId "
            "JOIN StudyGroups sg ON ss.StudyGroupId = sg.Id "
        "WHERE "
            "sg.Id = :studyId"
    );

    subjectForStudyGroup.bindValue(":studyId", studyGroupId);

    Execute(subjectForStudyGroup);

    const auto record = subjectForStudyGroup.record();

    const auto idIdx = record.indexOf("Id");

    auto result = QList<Id>();

    while (subjectForStudyGroup.next())
    {
        result.append(subjectForStudyGroup.value(idIdx).toInt());
    }

    return result;
}

QList<Subject> SubjectRepository::GetCompleteSubjectsForStudyGroup(const Id studyGroupId)
{
    QSqlQuery subjectForStudyGroup(m_context);
    subjectForStudyGroup.prepare(
        "SELECT s.Id, s.Abbrevation, s.Title, s.Completion, s.Weeks, s.LectureHours, s.ExerciseHours, s.SeminarHours, s.Language, s.GroupCapacity FROM "
            "Subjects s "
            "JOIN Subjects_StudyGroups ss ON s.Id = ss.SubjectId "
            "JOIN StudyGroups sg ON ss.StudyGroupId = sg.Id "
        "WHERE "
            "sg.Id = :studyId"
    );

    subjectForStudyGroup.bindValue(":studyId", studyGroupId);

    Execute(subjectForStudyGroup);

    const auto record = subjectForStudyGroup.record();

    const auto idIdx = record.indexOf("Id");
    const auto abbrevationIdx = record.indexOf("Abbrevation");
    const auto titleIdx = record.indexOf("Title");
    const auto weeksIdx = record.indexOf("Weeks");
    const auto lectureHoursIdx = record.indexOf("LectureHours");
    const auto exerciseHoursIdx = record.indexOf("ExerciseHours");
    const auto seminarHoursIdx = record.indexOf("SeminarHours");
    const auto languageIdx = record.indexOf("Language");
    const auto groupCapacityIdx = record.indexOf("GroupCapacity");

    auto result = QList<Subject>();

    while (subjectForStudyGroup.next())
    {
        auto sg = Subject();
        sg.m_id = subjectForStudyGroup.value(idIdx).toInt();
        sg.m_abbrevation = subjectForStudyGroup.value(abbrevationIdx).toString();
        sg.m_title = subjectForStudyGroup.value(titleIdx).toString();
        sg.m_weeks = subjectForStudyGroup.value(weeksIdx).toInt();
        sg.m_lectureHours = subjectForStudyGroup.value(lectureHoursIdx).toInt();
        sg.m_exerciseHours = subjectForStudyGroup.value(exerciseHoursIdx).toInt();
        sg.m_seminarHours = subjectForStudyGroup.value(seminarHoursIdx).toInt();
        sg.m_language = EnumMapper::ToLanguageEnum(subjectForStudyGroup.value(languageIdx).toString());
        sg.m_groupCapacity = subjectForStudyGroup.value(groupCapacityIdx).toInt();
        result.append(sg);
    }
    return result;
}
