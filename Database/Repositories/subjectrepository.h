#ifndef SUBJECTREPOSITORY_H
#define SUBJECTREPOSITORY_H

#include "Database_global.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSql>
#include "IRepository.h"
#include "../Models/subject.h"

#include <set>

#include "Utils/enummapper.h"


class DATABASE_EXPORT SubjectRepository: public IRepository<Subject>
{
public:
    SubjectRepository(QSqlDatabase context);

    QList<Subject> GetAll() override;
    void Remove(const int id) override;
    Id Add(const Subject* subject) override;
    bool Exists(const Subject* subject) override;
    int Count() override;
    void ConnectSubjectsToStudyGroups(const std::list<Id>& subjectIds, const std::list<Id>& studyGroupIds);
    void ConnectSubjectsToStudyGroups(const Id subjectId, const std::list<Id>& studyGroupIds);
    void ConnectSubjectsToStudyGroups(const std::list<Id> subjectIds, const Id studyGroupId);
    void Update(const Subject* subject) override;
    void DisconnectSubjectFromStudyGroups(const Subject* subject);

    QList<Id> GetSubjectsForStudyGroup(const Id studyGroupId);
    QList<Subject> GetCompleteSubjectsForStudyGroup(const Id studyGroupId);
protected:
    void CreateTable() override;
    void CreateDependentTables() override;
};

#endif // SUBJECTREPOSITORY_H
