QT -= gui
QT += sql core

TEMPLATE = lib
DEFINES += DATABASE_LIBRARY

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Database/database.cpp \
    Models/employee.cpp \
    Models/studygroup.cpp \
    Repositories/employeerepository.cpp \
    Repositories/studygrouprepository.cpp \
    Repositories/subjectrepository.cpp \
    Repositories/workitemrepository.cpp \
    Utils/enummapper.cpp

HEADERS += \
    Database_global.h \
    Database/database.h \
    Enums/Completion.h \
    Enums/Language.h \
    Enums/Semester.h \
    Enums/StudyForm.h \
    Enums/StudyType.h \
    Enums/WorkItemType.h \
    Models/employee.h \
    Models/model.h \
    Models/studygroup.h \
    Models/subject.h \
    Models/workitem.h \
    Models/workpoints.h \
    Repositories/IRepository.h \
    Repositories/employeerepository.h \
    Repositories/studygrouprepository.h \
    Repositories/subjectrepository.h \
    Repositories/workitemrepository.h \
    Utils/enummapper.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
