#include "database.h"

static const QString IN_MEMORY = ":memory:";

Database* Database::CreateInMemorySqlite()
{
    return new Database(IN_MEMORY);
}

Database::Database(const QString path, const QString type)
{
    if (!QSqlDatabase::isDriverAvailable(type))
    {
        qDebug() << "Database driver " << type << "is not available";
        throw std::invalid_argument("Unsupported DB driver");
    }

    m_db = QSqlDatabase::addDatabase(type, "SQLITE");
    m_db.setDatabaseName(path);

    {

    }
}

QSqlDatabase Database::Context()
{
    qDebug() << m_db;
    return m_db;
}

bool Database::open()
{
    const auto success = m_db.open();
    if (!success)
    {
        qDebug() << "Unable to open DB: '" << m_db.lastError().text() << "'";
        return success;
    }

    ConfigureDB();

    return success;
}

void Database::close()
{
    m_db.close();
}

bool Database::isOpen()
{
    return m_db.isOpen();
}

Database::~Database()
{
    close();
}

void Database::ConfigureDB()
{
    QSqlQuery query(m_db);
    query.exec("PRAGMA foreign_keys = ON;");
}


