#ifndef DATABASE_H
#define DATABASE_H

#include "Database_global.h"
#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QSqlQuery>
#include <QDebug>

static const QString DEFAULT_DATABASE_TYPE = "QSQLITE";

class DATABASE_EXPORT Database
{
public:
    static Database* CreateInMemorySqlite();
    Database(const QString path, const QString type = DEFAULT_DATABASE_TYPE);
    QSqlDatabase Context();
    bool open();
    void close();
    bool isOpen();
    ~Database();

private:
    void ConfigureDB();
    QSqlDatabase m_db;
};

#endif // DATABASE_H
