#include "enummapper.h"

QString EnumMapper::ToString(Completion comp)
{
    const auto result = CompletionValues.find(comp);

    return result != CompletionValues.end()
        ? result->second
        : "";
}

QString EnumMapper::ToString(Language lang)
{
    const auto result = LanguageValues.find(lang);

    return result != LanguageValues.end()
        ? result->second
        : "";
}

QString EnumMapper::ToString(Semester semester)
{
    const auto result = SemesterValues.find(semester);

    return result != SemesterValues.end()
        ? result->second
        : "";
}

QString EnumMapper::ToString(StudyForm form)
{
    const auto result = StudyFormValues.find(form);

    return result != StudyFormValues.end()
        ? result->second
        : "";
}

QString EnumMapper::ToString(StudyType type)
{
    const auto result = StudyTypeValues.find(type);

    return result != StudyTypeValues.end()
        ? result->second
        : "";
}

QString EnumMapper::ToString(WorkItemType type)
{
    const auto result = WorkItemTypeValues.find(type);

    return result != WorkItemTypeValues.end()
        ? result->second
        : "";
}

Completion EnumMapper::ToCompletionEnum(const QString &str)
{
    for (const auto& record: CompletionValues)
    {
        if (record.second == str)
            return record.first;
    }
    throw std::runtime_error("Invalid completion value");
}

Language EnumMapper::ToLanguageEnum(const QString &str)
{
    for (const auto& record: LanguageValues)
    {
        if (record.second == str)
            return record.first;
    }
    throw std::runtime_error("Invalid Language value");
}

Semester EnumMapper::ToSemesterEnum(const QString &str)
{
    for (const auto& record: SemesterValues)
    {
        if (record.second == str)
            return record.first;
    }
    throw std::runtime_error("Invalid Semester value");
}

StudyForm EnumMapper::ToStudyFormEnum(const QString &str)
{
    for (const auto& record: StudyFormValues)
    {
        if (record.second == str)
            return record.first;
    }
    throw std::runtime_error("Invalid study form value");
}

StudyType EnumMapper::ToStudyTypeEnum(const QString &str)
{
    for (const auto& record: StudyTypeValues)
    {
        if (record.second == str)
            return record.first;
    }
    throw std::runtime_error("Invalid study type value");
}

WorkItemType EnumMapper::ToWorkItemTypeEnum(const QString &str)
{
    for (const auto& record: WorkItemTypeValues)
    {
        if (record.second == str)
            return record.first;
    }
    throw std::runtime_error("Invalid work item type value");
}

WorkItemType EnumMapper::MapToWorkItemType(const Completion comp)
{
    switch(comp)
    {
        case Completion::ClassifiedCredit:
            return WorkItemType::ClassifiedCredit;
        case Completion::Credit:
            return WorkItemType::Credit;
        case Completion::Exam:
            return WorkItemType::Exam;
    }

    return WorkItemType::Exam;
}

