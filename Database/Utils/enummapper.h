#ifndef ENUMMAPPER_H
#define ENUMMAPPER_H

#include "Database_global.h"

#include "Enums/Completion.h"
#include "Enums/Language.h"
#include "Enums/Semester.h"
#include "Enums/StudyForm.h"
#include "Enums/StudyType.h"
#include "Enums/WorkItemType.h"

#include <QString>

class DATABASE_EXPORT EnumMapper
{
public:
    static QString ToString(Completion comp);
    static QString ToString(Language lang);
    static QString ToString(Semester semester);
    static QString ToString(StudyForm form);
    static QString ToString(StudyType type);
    static QString ToString(WorkItemType type);

    static Completion ToCompletionEnum(const QString& str);
    static Language ToLanguageEnum(const QString& str);
    static Semester ToSemesterEnum(const QString& str);
    static StudyForm ToStudyFormEnum(const QString& str);
    static StudyType ToStudyTypeEnum(const QString& str);
    static WorkItemType ToWorkItemTypeEnum(const QString& str);

    static WorkItemType MapToWorkItemType(const Completion comp);
private:
    EnumMapper() = default;
};

#endif // ENUMMAPPER_H
