#ifndef WORKITEMTYPE_H
#define WORKITEMTYPE_H

#include <QString>
#include <QStringList>
#include <map>

enum class WorkItemType {
    Lecture,
    Exercise,
    Seminar,
    Exam,
    Credit,
    ClassifiedCredit
};

static const auto WorkItemTypeValueList = QStringList{
    "Lecture",
    "Exercise",
    "Seminar",
    "Exam",
    "Credit",
    "Classified credit"
};

static const std::map<WorkItemType, QString> WorkItemTypeValues = {
    { WorkItemType::Lecture, "Lecture" },
    { WorkItemType::Exercise, "Exercise" },
    { WorkItemType::Seminar, "Seminar" },
    { WorkItemType::Exam, "Exam" },
    { WorkItemType::Credit, "Credit" },
    { WorkItemType::ClassifiedCredit, "Classified credit"}
};

#endif // WORKITEMTYPE_H
