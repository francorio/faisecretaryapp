#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <QString>
#include <QStringList>
#include <map>

enum class Language { CZ, EN };

static const auto LanguageValueList = QStringList{
    "CZ",
    "EN"
};

static const std::map<Language, QString> LanguageValues = {
    { Language::CZ, "CZ" },
    { Language::EN, "EN"}
};


#endif // LANGUAGE_H
