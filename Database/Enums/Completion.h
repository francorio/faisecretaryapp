#ifndef COMPLETION_H
#define COMPLETION_H

#include <QString>
#include <QStringList>
#include <map>

enum class Completion { Exam, Credit, ClassifiedCredit };
static const auto CompletionValueList = QStringList{
    "Exam",
    "Classified credit",
    "Credit"
};

static const std::map<Completion, QString> CompletionValues = {
    { Completion::Exam, "Exam" },
    { Completion::ClassifiedCredit, "Classified credit"},
    { Completion::Credit, "Credit" }
};

#endif // COMPLETION_H
