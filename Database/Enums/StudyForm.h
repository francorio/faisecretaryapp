#ifndef STUDYFORM_H
#define STUDYFORM_H

#include <QString>
#include <QStringList>
#include <map>

enum class StudyForm { FullTime, PartTime };
static const auto StudyFormValueList = QStringList{
    "Full-time",
    "Part-time"
};

static const std::map<StudyForm, QString> StudyFormValues = {
    { StudyForm::FullTime, "Full-time" },
    { StudyForm::PartTime, "Part-time" }
};

#endif // STUDYFORM_H
