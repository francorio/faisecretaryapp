#ifndef STUDYTYPE_H
#define STUDYTYPE_H

#include <QString>
#include <QStringList>
#include <map>

enum class StudyType { Bachelor, Master };
static const auto StudyTypeValueList = QStringList{
    "Bachelor",
    "Master"
};

static const std::map<StudyType, QString> StudyTypeValues = {
    { StudyType::Bachelor, "Bachelor" },
    { StudyType::Master, "Master" }
};

#endif // STUDYTYPE_H
