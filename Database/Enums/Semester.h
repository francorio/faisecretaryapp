#ifndef SEMESTER_H
#define SEMESTER_H

#include <QString>
#include <QStringList>
#include <map>

enum class Semester { Spring, Fall };

static const auto SemesterValueList = QStringList{
    "Fall",
    "Spring"
};

static const std::map<Semester, QString> SemesterValues = {
    { Semester::Fall, "Fall" },
    { Semester::Spring, "Spring"}
};

#endif // SEMESTER_H
