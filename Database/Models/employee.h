#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <QString>
#include "Model.h"

#include "Database_global.h"


class DATABASE_EXPORT Employee: public Model
{
public:
    Employee() = default;
    Employee(int id): Model(id) {};
    QString m_name;
    QString m_surname;
    QString m_workEmail;
    QString m_personalEmail;
    QString m_workPhone;
    QString m_personalPhone;
    int m_CZWorkPoints = 0;
    int m_ENWorkPoints = 0;
    int m_isInStudy = 0;
    double m_workLoad = 0.0;
};

#endif // EMPLOYEE_H
