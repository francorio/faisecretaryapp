#ifndef WORKPOINTS_H
#define WORKPOINTS_H

#include "Model.h"
#include "Database_global.h"

class DATABASE_EXPORT WorkPoints: public Model
{
public:
    WorkPoints() = default;
    double CZPoints;
};

#endif // WORKPOINTS_H
