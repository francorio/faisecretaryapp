#ifndef SUBJECT_H
#define SUBJECT_H

#include <QString>
#include "Model.h"

#include "Enums/Language.h"
#include "Enums/Completion.h"

#include "Database_global.h"

class DATABASE_EXPORT Subject: public Model
{
public:
    Subject() = default;
    Subject(int id): Model(id) {};
    QString m_abbrevation;
    QString m_title;
    Completion m_completion;
    int m_weeks = 0;
    int m_lectureHours = 0;
    int m_seminarHours = 0;
    int m_exerciseHours = 0;
    Language m_language;
    int m_groupCapacity = 0;
};

#endif // SUBJECT_H
