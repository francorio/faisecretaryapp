#ifndef STUDYGROUP_H
#define STUDYGROUP_H

#include "Model.h"
#include <QString>

#include "Enums/Language.h"
#include "Enums/StudyForm.h"
#include "Enums/StudyType.h"
#include "Enums/Semester.h"

#include "Database_global.h"

class DATABASE_EXPORT StudyGroup: public Model
{
public:
    StudyGroup() = default;
    StudyGroup(int id): Model(id) {};
    QString m_abbrevation;
    StudyForm m_studyForm;
    int m_enrolled = 0;
    Semester m_semester;
    int m_year = 0;
    Language m_language;
    StudyType m_type;
};

#endif // STUDYGROUP_H
