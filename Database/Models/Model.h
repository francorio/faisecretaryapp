#ifndef MODEL_H
#define MODEL_H

#include "Database_global.h"


class DATABASE_EXPORT Model
{
public:
    Model() = default;
    Model(int id): m_id(id) {};
    bool operator==(const Model& left) { return m_id == left.m_id; };
    int m_id;
};

#endif // MODEL_H
