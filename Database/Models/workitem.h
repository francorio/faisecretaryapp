#ifndef WORKITEM_H
#define WORKITEM_H

#include <QString>
#include "Model.h"

#include "Enums/WorkItemType.h"
#include "Enums/Language.h"

#include "Database_global.h"

class DATABASE_EXPORT WorkItem: public Model
{
public:
    WorkItem() = default;
    WorkItem(int id): Model(id) {};
    QString m_name;
    WorkItemType m_type;
    int m_employeeId = 0;
    int m_subjectId = 0;
    int m_numberOfStudents = 0;
    int m_numberOfHours = 0;
    int m_numberOfWeeks = 0;
    Language m_language;
    bool m_isValid = true;
};

#endif // WORKITEM_H
