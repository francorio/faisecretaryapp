#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setFixedSize(600, 350);
    statusBar()->showMessage("Started", 2000);

    m_tabWidget = new QTabWidget(this);

//    m_db = std::shared_ptr<Database>(Database::CreateInMemorySqlite());
    m_db = std::make_shared<Database>("ftest.sqlite");

    if (!m_db->open())
    {
        statusBar()->showMessage("Connect to DB: Failed", 2000);
        return;
    }
    statusBar()->showMessage("Connect to DB: Success", 2000);


    setCentralWidget(m_tabWidget);

    auto employeeRepository = std::make_shared<EmployeeRepository>(m_db->Context());
    auto studyGroupRepository = std::make_shared<StudyGroupRepository>(m_db->Context());
    auto subjectRepository = std::make_shared<SubjectRepository>(m_db->Context());
    auto workItemRepository = std::make_shared<WorkItemRepository>(m_db->Context());
    


    auto employee = new EmployeeWidget(
        employeeRepository,
        workItemRepository,
        m_tabWidget
    );
    auto studyGroup = new StudyGroupWidget(
        studyGroupRepository,
        subjectRepository,
        m_tabWidget
    );
    auto subject = new SubjectWidget(
        subjectRepository,
        studyGroupRepository,
        m_tabWidget
    );
    auto workItem = new WorkItemWidget(
        workItemRepository,
        employeeRepository,
        subjectRepository,
        studyGroupRepository,
        m_tabWidget
    );

    const auto employeeIdx = m_tabWidget->addTab(employee, "Employees");
    const auto studyGroupIdx = m_tabWidget->addTab(studyGroup, "Study groups");
    const auto subjectIdx = m_tabWidget->addTab(subject, "Subjects");
    const auto workItemIdx = m_tabWidget->addTab(workItem, "Work items");

    m_tabWidget->setCurrentIndex(0);

    m_tabs[employeeIdx] = employee;
    m_tabs[studyGroupIdx] = studyGroup;
    m_tabs[subjectIdx] = subject;
    m_tabs[workItemIdx] = workItem;

    connect(
        m_tabWidget, &QTabWidget::currentChanged,
        this, &MainWindow::RefreshTab
    );

    QKeySequence keys_refresh(Qt::Key_F5);
    QAction* actionReload = new QAction(this);
    actionReload->setShortcut(keys_refresh);
    QObject::connect(
        actionReload, &QAction::triggered,
        this, [this]() { RefreshTab(m_tabWidget->currentIndex()); }
    );

    addAction(actionReload);
}

MainWindow::~MainWindow()
{
    if (m_db->isOpen())
    {
        m_db->close();
    }
}

void MainWindow::RefreshTab(const int index)
{
    if (index == -1)
        return;
    m_tabs[index]->OnRefresh();
}

