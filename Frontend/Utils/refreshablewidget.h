#ifndef IWIDGET_H
#define IWIDGET_H

#include <QWidget>

class RefreshableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RefreshableWidget(QWidget *parent = nullptr);
    virtual void OnRefresh() = 0;
protected:
    virtual void Connect() = 0;
};

#endif // IWIDGET_H
