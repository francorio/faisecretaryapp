#ifndef ROWITEM_H
#define ROWITEM_H

#include <QWidget>
#include <QHBoxLayout>

#include "Models/Model.h"

class RowItem : public QWidget
{
    Q_OBJECT
public:
    explicit RowItem(const Model* model, QWidget *parent = nullptr);

    int m_id;
protected:
    QHBoxLayout* m_layout;
};

#endif // ROWITEM_H
