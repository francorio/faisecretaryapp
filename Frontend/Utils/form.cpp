#include "form.h"

Form::Form(QWidget *parent)
    : QWidget(parent)
{
    m_layout = new QFormLayout(this);
    m_formHasErrorLabel = new QLabel(this);
    m_formHasErrorLabel->setText("None");

    m_layout->addRow(new QLabel("Error in form:"), m_formHasErrorLabel);
}

bool Form::IsValid() const
{
    for (const auto& i: m_widgetValidation)
    {
        const auto validation = i.second;
        const auto name = i.first.first;
        const auto errorGetter = i.first.second;

        if (!validation())
        {
            m_formHasErrorLabel->setText(name + ": " + errorGetter->Error());
            return false;
        }
    }
    m_formHasErrorLabel->setText("None");
    return true;
}

void Form::AddTextEdit(
    const QString &labelText,
    QString &store,
    std::shared_ptr<IValidator> validator
)
{
    const auto editor = new QLineEdit(store, this);
    m_layout->addRow(new QLabel(labelText, this), editor);
    connect(
        editor, &QLineEdit::textChanged,
        this, [this, labelText, editor, &store, validator]()
        {
            if (validator->Validate(editor->text()))
            {
                store = editor->text();
                m_formHasErrorLabel->setText("None");
            }
            else
            {
                m_formHasErrorLabel->setText(labelText);
            }
    });

    m_widgetValidation.push_back({{labelText, validator}, [editor, validator]() { return validator->Validate(editor->text()); }});
}

void Form::AddTextEdit(
    const QString &labelText,
    const QString &actual,
    const std::function<void (QString)> &saveValue,
    std::shared_ptr<IValidator> validator
)
{
    const auto editor = new QLineEdit(actual, this);
    m_layout->addRow(new QLabel(labelText, this), editor);
    connect(
        editor, &QLineEdit::textChanged,
        this, [this, labelText, editor, saveValue, validator]()
        {
            const auto text = editor->text();
            if (validator->Validate(text))
            {
                saveValue(text);
                m_formHasErrorLabel->setText("None");
            }
            else
            {
                m_formHasErrorLabel->setText(labelText);
            }
    });
    m_widgetValidation.push_back({{labelText, validator}, [editor, validator]() { return validator->Validate(editor->text()); }});
}

void Form::AddTextEdit(
    const QString &labelText,
    QString &store
)
{
    const auto editor = new QLineEdit(store, this);
    m_layout->addRow(new QLabel(labelText, this), editor);
    connect(
        editor, &QLineEdit::textChanged,
        this, [editor, &store]()
        {
            store = editor->text();
        }
    );

    const std::shared_ptr<IValidator> validator = std::make_shared<Validator>();

    m_widgetValidation.push_back({{labelText, validator}, []() { return true; }});
}

void Form::AddCombobox(
    const QString &labelText,
    const QString &actual,
    const QStringList &values,
    const std::function<void(QString)> &saveEnumValue
)
{
    const auto combo = new QComboBox(this);
    combo->addItems(values);
    combo->setCurrentText(actual);
    m_layout->addRow(new QLabel(labelText, this), combo);
    connect(
        combo, &QComboBox::currentTextChanged,
        this, [saveEnumValue, combo]()
        {
            saveEnumValue(combo->currentText());
        }
    );
}

void Form::ShowTooltip(const QPoint &where, const QString &what)
{
    QToolTip::showText(where, what);
}
