#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QLabel>
#include <QToolTip>
#include "validator.h"

class Form : public QWidget
{
    Q_OBJECT
public:
    explicit Form(QWidget *parent = nullptr);
    bool IsValid() const;

protected:


    virtual void AddTextEdit(
        const QString& labelText,
        QString& store,
        std::shared_ptr<IValidator> validator
    );

    virtual void AddTextEdit(
        const QString& labelText,
        const QString& actual,
        const std::function<void(QString)>& saveValue,
        std::shared_ptr<IValidator> validator
    );

    virtual void AddTextEdit(
        const QString& labelText,
        QString& store
    );
    virtual void AddCombobox(
        const QString& labelText,
        const QString& actual,
        const QStringList& values,
        const std::function<void(QString)>& saveEnumValue
    );

    void ShowTooltip(const QPoint& where, const QString& what);

    QFormLayout* m_layout;
    QLabel* m_formHasErrorLabel;

    using ValidationFunction = std::function<bool(void)>;
    using NameAndValidation = std::pair<QString, std::shared_ptr<IValidator>>;
    using WidgetValidation = std::pair<NameAndValidation, ValidationFunction>;

    std::vector<WidgetValidation> m_widgetValidation;
};

#endif // FORM_H
