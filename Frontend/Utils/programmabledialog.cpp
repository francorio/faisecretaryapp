#include "programmabledialog.h"

static const QString DEFAULT_ACCEPT_TEXT = "Accept";
static const QString DEFAULT_CANCEL_TEXT = "Cancel";

static const auto DEFAULT_ACTION = []() { return true; };

ProgrammableDialog::ProgrammableDialog(QWidget* toDisplay, QWidget* parent)
    : QDialog(parent, Qt::WindowCloseButtonHint)
{
    m_actions.resize(Operation::COUNT);
    SetUpDefaultActions();

    const auto layout = new QVBoxLayout(this);

    layout->addWidget(toDisplay);

    {
        m_buttons.resize(2);
        for (auto i = 0; i < Operation::COUNT; ++i)
        {
            m_buttons[i] = new QPushButton(this);
        }

        m_buttons[Operation::ACCEPT]->setText(DEFAULT_ACCEPT_TEXT);
        m_buttons[Operation::CANCEL]->setText(DEFAULT_CANCEL_TEXT);
    }

    const auto buttonsWidget = new QWidget(this);

    {
        const auto buttonsLayout = new QHBoxLayout(buttonsWidget);
        std::for_each(
            m_buttons.begin(),
            m_buttons.end(),
            [l = buttonsLayout](QPushButton* btn)
            {
                l->addWidget(btn);
            }
        );

        layout->addWidget(buttonsWidget);
        layout->setAlignment(buttonsWidget, Qt::AlignBottom);
    }

    connect(
        m_buttons[Operation::ACCEPT], &QPushButton::clicked,
        this, &ProgrammableDialog::OnAcceptClicked
    );

    connect(
        m_buttons[Operation::CANCEL], &QPushButton::clicked,
        this, &ProgrammableDialog::OnCancelClicked
    );

    setMinimumSize(200, 200);
}

void ProgrammableDialog::SetAcceptButtonText(const QString &text)
{
    SetButtonText(Operation::ACCEPT, text);
}

void ProgrammableDialog::SetCancelButtonText(const QString &text)
{
    SetButtonText(Operation::CANCEL, text);
}

void ProgrammableDialog::OnAccept(const ProgrammableDialog::Action &action)
{
    m_actions[Operation::ACCEPT] = action;
}

void ProgrammableDialog::OnCancel(ProgrammableDialog::Action &action)
{
    m_actions[Operation::CANCEL] = action;
}

void ProgrammableDialog::SetUpDefaultActions()
{
    std::for_each(m_actions.begin(), m_actions.end(), [](Action & act) { act = DEFAULT_ACTION; });
}

void ProgrammableDialog::SetButtonText(const Operation operation, const QString &text)
{
    m_buttons[operation]->setText(text);
}

void ProgrammableDialog::OnAcceptClicked()
{
    const bool success = m_actions[Operation::ACCEPT]();

    if (success)
    {
        setResult(QDialog::DialogCode::Accepted);
        close();
    }
}

void ProgrammableDialog::OnCancelClicked()
{
    const bool success = m_actions[Operation::CANCEL]();
    if (success)
    {
        setResult(QDialog::DialogCode::Rejected);
        close();
    }
}
