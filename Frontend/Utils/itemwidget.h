#ifndef ITEMWIDGET_H
#define ITEMWIDGET_H

#include <QtWidgets>
#include "refreshablewidget.h"
#include "Utils/form.h"
#include "Models/Model.h"

class ItemWidget : public RefreshableWidget
{
    Q_OBJECT
public:
    explicit ItemWidget(QWidget *parent = nullptr);

signals:
    void itemSelected(const bool b);

    // RefreshableWidget interface
public:
    void mousePressEvent(QMouseEvent* event) override;
    void OnRefresh() override;

protected:
    virtual void DBRefresh() = 0;
    virtual void UIRefresh() = 0;
    void Connect() override;

    virtual void CreateLayout();
    void CreateListWidget();
    void CreateDefaultButtons();
    void CreateSidePanel();

    virtual void EnableOperationButtons(const bool b);

    virtual void ClearContainer() = 0;

    void OnSelectedItem(QListWidgetItem* item);

    virtual void OnAddClicked() = 0;
    virtual void OnEditClicked() = 0;
    void OnDeleteClicked();

    virtual void SaveSelectedObject() = 0;
    virtual void ResetSelectedObject();

    virtual void RemoveSelectedObject() = 0;

    virtual bool OnAddNewObject(const Model* model, const Form* form) = 0;
    virtual bool OnEditObject(const Model* model, const Form* form) = 0;

    virtual bool CanCreateNew(const Model* model) = 0;


    QHBoxLayout* m_mainLayout;


    QListWidget* m_list;
    QListWidgetItem* m_selectedItem;

    QVBoxLayout* m_sidePanel;


    enum DefaultButtons: int { ADD = 0, EDIT, REMOVE, COUNT };
    std::vector<QPushButton*> m_defaultButtons;
};

#endif // ITEMWIDGET_H
