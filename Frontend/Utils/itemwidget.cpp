#include "itemwidget.h"

ItemWidget::ItemWidget(QWidget *parent)
    : RefreshableWidget(parent)
{
    m_defaultButtons.resize(DefaultButtons::COUNT);

    CreateLayout();

    EnableOperationButtons(false);
}

void ItemWidget::mousePressEvent(QMouseEvent *event)
{
    if (!m_list->underMouse() && m_selectedItem)
    {
        m_selectedItem->setSelected(false);
        ResetSelectedObject();
        emit itemSelected(false);
    }
    else
    {
        RefreshableWidget::mousePressEvent(event);
    }
}

void ItemWidget::OnRefresh()
{
    ResetSelectedObject();
    ClearContainer();
    DBRefresh();
    UIRefresh();
}

void ItemWidget::Connect()
{
    connect(
        m_list, &QListWidget::itemClicked,
        this, &ItemWidget::OnSelectedItem
    );

    connect(
        this, &ItemWidget::itemSelected,
        this, &ItemWidget::EnableOperationButtons
    );

    connect(
        m_defaultButtons[DefaultButtons::ADD], &QPushButton::clicked,
        this, &ItemWidget::OnAddClicked
    );

    connect(
        m_defaultButtons[DefaultButtons::EDIT], &QPushButton::clicked,
        this, &ItemWidget::OnEditClicked
    );

    connect(
        m_defaultButtons[DefaultButtons::REMOVE], &QPushButton::clicked,
        this, &ItemWidget::OnDeleteClicked
            );
}

void ItemWidget::CreateLayout()
{
    m_mainLayout = new QHBoxLayout(this);
    auto listButtonsLayout = new QVBoxLayout;

    CreateListWidget();
    CreateDefaultButtons();


    listButtonsLayout->addWidget(m_list);
    {
        auto buttonsLayout = new QHBoxLayout;

        buttonsLayout->addWidget(m_defaultButtons[DefaultButtons::ADD]);

        buttonsLayout->addSpacing(20);
        buttonsLayout->setAlignment(Qt::AlignRight);
        buttonsLayout->addWidget(m_defaultButtons[DefaultButtons::EDIT]);
        buttonsLayout->addWidget(m_defaultButtons[DefaultButtons::REMOVE]);

        listButtonsLayout->addLayout(buttonsLayout);
    }

    m_mainLayout->addLayout(listButtonsLayout);

    CreateSidePanel();
}

void ItemWidget::CreateListWidget()
{
    m_list = new QListWidget(this);
    m_list->setSelectionMode(QAbstractItemView::SingleSelection);
    m_selectedItem = nullptr;
}

void ItemWidget::CreateDefaultButtons()
{
    static const auto labels = QStringList{ "Add", "Edit", "Delete" };

    for (auto index = 0; index < DefaultButtons::COUNT; ++index)
    {
        m_defaultButtons[index] = new QPushButton(labels[index], this);
    }
}

void ItemWidget::CreateSidePanel()
{
    const auto sidePanel = new QGroupBox("Control tools", this);
    m_mainLayout->addWidget(sidePanel);

    m_sidePanel = new QVBoxLayout(sidePanel);
    m_sidePanel->setAlignment(Qt::AlignTop);

    sidePanel->setMinimumWidth(120);
}

void ItemWidget::EnableOperationButtons(const bool b)
{
    static const auto operationButtons = std::vector<DefaultButtons>{ DefaultButtons::EDIT, DefaultButtons::REMOVE };

    std::for_each(
        operationButtons.begin(),
        operationButtons.end(),
        [this, b](const auto btn)
        {
            m_defaultButtons[btn]->setEnabled(b);
        });
}

void ItemWidget::OnSelectedItem(QListWidgetItem *item)
{
    m_selectedItem = item;
    SaveSelectedObject();
    emit itemSelected(true);
}

void ItemWidget::OnDeleteClicked()
{
    if (!m_selectedItem)
    {
        return;
    }

    RemoveSelectedObject();
    ResetSelectedObject();
}

void ItemWidget::ResetSelectedObject()
{
    m_selectedItem = nullptr;
}
