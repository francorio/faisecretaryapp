#ifndef PROGRAMMABLEDIALOG_H
#define PROGRAMMABLEDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>

class ProgrammableDialog : public QDialog
{
    Q_OBJECT
    enum Operation: int { ACCEPT = 0, CANCEL = 1, COUNT };
public:
    using Action = std::function<bool(void)>;
    ProgrammableDialog(QWidget* toDisplay, QWidget* parent = nullptr);
    void SetAcceptButtonText(const QString& text);
    void SetCancelButtonText(const QString& text);

    void OnAccept(const Action& action);
    void OnCancel(Action& action);

private:
    void SetUpDefaultActions();
    void SetButtonText(const Operation operation, const QString& text);
    void OnAcceptClicked();
    void OnCancelClicked();

    std::vector<QPushButton*> m_buttons;
    std::vector<Action> m_actions;
};

#endif // PROGRAMMABLEDIALOG_H
