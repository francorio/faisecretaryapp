QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Employee/employeerowitem.cpp \
    StudyGroup/studygroupform.cpp \
    StudyGroup/studygrouprowitem.cpp \
    StudyGroup/studygroupwidget.cpp \
    Subject/subjectform.cpp \
    Subject/subjectrowitem.cpp \
    Subject/subjectwidget.cpp \
    Utils/form.cpp \
    Utils/itemwidget.cpp \
    Utils/rowitem.cpp \
    WorkItem/workitemform.cpp \
    WorkItem/workitemrowitem.cpp \
    WorkItem/workitemwidget.cpp \
    lineedit.cpp \
    main.cpp \
    mainwindow.cpp \
    Employee/employeeform.cpp \
    Employee/employeewidget.cpp \
    Utils/programmabledialog.cpp \
    Utils/refreshablewidget.cpp

HEADERS += \
    Employee/employeerowitem.h \
    StudyGroup/studygroupform.h \
    StudyGroup/studygrouprowitem.h \
    StudyGroup/studygroupwidget.h \
    Subject/subjectform.h \
    Subject/subjectrowitem.h \
    Subject/subjectwidget.h \
    Utils/form.h \
    Utils/itemwidget.h \
    Utils/rowitem.h \
    WorkItem/workitemform.h \
    WorkItem/workitemrowitem.h \
    WorkItem/workitemwidget.h \
    lineedit.h \
    mainwindow.h \
    Employee/employeeform.h \
    Employee/employeewidget.h \
    Utils/programmabledialog.h \
    Utils/refreshablewidget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Database/release/ -lDatabase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Database/debug/ -lDatabase
else:unix: LIBS += -L$$OUT_PWD/../Database/ -lDatabase

INCLUDEPATH += $$PWD/../Database
DEPENDPATH += $$PWD/../Database

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Validation/release/ -lValidation
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Validation/debug/ -lValidation
else:unix: LIBS += -L$$OUT_PWD/../Validation/ -lValidation

INCLUDEPATH += $$PWD/../Validation
DEPENDPATH += $$PWD/../Validation

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GeneratorLogic/release/ -lGeneratorLogic
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GeneratorLogic/debug/ -lGeneratorLogic
else:unix: LIBS += -L$$OUT_PWD/../GeneratorLogic/ -lGeneratorLogic

INCLUDEPATH += $$PWD/../GeneratorLogic
DEPENDPATH += $$PWD/../GeneratorLogic
