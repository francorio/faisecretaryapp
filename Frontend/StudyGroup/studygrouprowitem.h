#ifndef STUDYGROUPROWITEM_H
#define STUDYGROUPROWITEM_H

#include "Utils/rowitem.h"
#include "Models/studygroup.h"
#include <QLabel>
#include "Utils/enummapper.h"

class StudyGroupRowItem: public RowItem
{
public:
    StudyGroupRowItem(const StudyGroup* m, QWidget* parent = nullptr);
};

#endif // STUDYGROUPROWITEM_H
