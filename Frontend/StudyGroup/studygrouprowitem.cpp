#include "studygrouprowitem.h"

StudyGroupRowItem::StudyGroupRowItem(
    const StudyGroup* m,
    QWidget* parent
): RowItem(m, parent)
{
    const auto abbrevation = m->m_abbrevation;
    const auto semester = m->m_semester;
    const auto enrolled = m->m_enrolled;
    const auto language = m->m_language;
    const auto year = m->m_year;

    m_layout->addWidget(new QLabel(abbrevation, this));
    m_layout->addWidget(new QLabel(EnumMapper::ToString(semester), this));
    m_layout->addWidget(new QLabel(EnumMapper::ToString(language), this));
    m_layout->addWidget(new QLabel(QString::number(year), this));
    m_layout->addWidget(new QLabel(QString::number(enrolled), this));
}
