#ifndef STUDYGROUPWIDGET_H
#define STUDYGROUPWIDGET_H

#include <QtWidgets>
#include <QPushButton>

#include "studygroupform.h"
#include "Utils/programmabledialog.h"

#include "Utils/itemwidget.h"
#include "Repositories/studygrouprepository.h"
#include "Repositories/subjectrepository.h"
#include "studygrouprowitem.h"

class StudyGroupWidget : public ItemWidget
{
    Q_OBJECT
public:
    explicit StudyGroupWidget(
        std::shared_ptr<StudyGroupRepository> studyGroupRepository,
        std::shared_ptr<SubjectRepository> subjectRepository,
        QWidget *parent = nullptr
    );

protected:
    void DBRefresh() override;
    void UIRefresh() override;
    void ClearContainer() override;
    void OnAddClicked() override;
    void OnEditClicked() override;
    void SaveSelectedObject() override;
    void ResetSelectedObject() override;
    void RemoveSelectedObject() override;

    bool OnAddNewObject(const Model *model, const Form *form) override;
    bool OnEditObject(const Model *model, const Form *form) override;
    bool CanCreateNew(const Model* model) override;

private:
    StudyGroup* Cast(const Model* model);
    StudyGroupForm* Cast(const Form* form);

    QList<StudyGroup> m_studyGroups;
    StudyGroupRowItem* m_selectedStudyGroup;

    std::shared_ptr<StudyGroupRepository> m_studyGroupRepository;
    std::shared_ptr<SubjectRepository> m_subjectRepository;
};

#endif // STUDYGROUPWIDGET_H
