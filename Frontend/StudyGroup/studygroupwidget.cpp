#include "studygroupwidget.h"

StudyGroupWidget::StudyGroupWidget(
    std::shared_ptr<StudyGroupRepository> studyGrouprepository,
    std::shared_ptr<SubjectRepository> subjectRepository,
    QWidget *parent
)
    : ItemWidget(parent), m_studyGroupRepository(studyGrouprepository), m_subjectRepository(subjectRepository)
{
    Connect();

    EnableOperationButtons(false);
    OnRefresh();
}

void StudyGroupWidget::DBRefresh()
{
    m_studyGroups = m_studyGroupRepository->GetAll();
}

void StudyGroupWidget::UIRefresh()
{
    for (const auto& f: m_studyGroups)
    {

        auto listItem = new QListWidgetItem(m_list);

        const auto itemWidget = new StudyGroupRowItem(&f, m_list);

        listItem->setSizeHint(itemWidget->sizeHint());

        m_list->addItem(listItem);

        m_list->setItemWidget(listItem, itemWidget);
    }
}

void StudyGroupWidget::ClearContainer()
{
    m_list->clear();
    m_studyGroups.clear();
}

void StudyGroupWidget::OnAddClicked()
{
    const auto newStudyGroup = new StudyGroup();
    const auto newStudyGroupForm = new StudyGroupForm(newStudyGroup, m_subjectRepository);

    const auto dialog = new ProgrammableDialog(newStudyGroupForm, this);
    dialog->SetAcceptButtonText("Add");

    auto shouldRefresh = false;

    const auto acceptFunction = ProgrammableDialog::Action(
        [this, newStudyGroup, newStudyGroupForm, &shouldRefresh]() -> bool
        {
            if (!newStudyGroupForm->IsValid())
                return false;

            if (OnAddNewObject(newStudyGroup, newStudyGroupForm))
            {
                shouldRefresh = true;
                return true;
            }
            else
            {
                QMessageBox mb(this);
                mb.setText("Study group already exists");
                mb.exec();
                return false;
            }
        }
    );

    dialog->OnAccept(acceptFunction);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }

}

void StudyGroupWidget::OnEditClicked()
{
    if (!m_selectedItem)
    {
        return;
    }

    const auto index = m_studyGroups.indexOf(StudyGroup(m_selectedStudyGroup->m_id));
    if (index == -1)
    {
        qDebug() << "Study group not found!";
        return;
    }
    auto selected = &m_studyGroups[index];

    const auto updateStudyGroupForm = new StudyGroupForm(selected, m_subjectRepository);

    const auto dialog = new ProgrammableDialog(updateStudyGroupForm, this);
    dialog->SetAcceptButtonText("Update");

    auto shouldRefresh = false;

    const auto onUpdate = ProgrammableDialog::Action(
        [this, &shouldRefresh, selected, updateStudyGroupForm]() -> bool
        {
            if (!updateStudyGroupForm->IsValid())
                return false;

            if (OnEditObject(selected, updateStudyGroupForm))
            {
                shouldRefresh = true;
                return true;
            }
            return false;
        }
    );

    dialog->OnAccept(onUpdate);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }

}

void StudyGroupWidget::ResetSelectedObject()
{
    ItemWidget::ResetSelectedObject();
    m_selectedStudyGroup = nullptr;
}

void StudyGroupWidget::SaveSelectedObject()
{
    m_selectedStudyGroup = dynamic_cast<StudyGroupRowItem*>(m_list->itemWidget(m_selectedItem));
}

void StudyGroupWidget::RemoveSelectedObject()
{
    {
        // UI list query
        const auto studyGroupToDelete = StudyGroup(m_selectedStudyGroup->m_id);
        m_studyGroups.removeOne(studyGroupToDelete);
    }
    {
        // Database query
        m_studyGroupRepository->Remove(m_selectedStudyGroup->m_id);
    }

    // Widget list query
    m_list->takeItem(m_list->currentRow());
}

bool StudyGroupWidget::OnAddNewObject(const Model *model, const Form *form)
{
    const auto studyGroup = Cast(model);
    const auto studyGroupForm = Cast(form);

    const auto canCreateNewStudyGroup = CanCreateNew(model);

    auto success = false;

    if (canCreateNewStudyGroup)
    {
        {
            const auto transaction = m_studyGroupRepository->GetTransaction();

            const auto studyGroupId = m_studyGroupRepository->Add(studyGroup);
            const auto selectedSubjects = studyGroupForm->GetSelectedSubjects();
            const auto listOfSubjects = std::list<Id>(selectedSubjects.begin(), selectedSubjects.end());
            m_subjectRepository->ConnectSubjectsToStudyGroups(listOfSubjects, studyGroupId);
        }
        success = true;
    }
    return success;
}

bool StudyGroupWidget::OnEditObject(const Model *model, const Form *form)
{
    const auto studyGroup = Cast(model);
    const auto studyGroupForm = Cast(form);

    {
        const auto transaction = m_subjectRepository->GetTransaction();
        m_studyGroupRepository->Update(studyGroup);
        m_studyGroupRepository->DisconnectStudyGroupFromSubjects(studyGroup);
        const auto subjects = studyGroupForm->GetSelectedSubjects();
        const auto listOfSubjects = std::list<Id>(subjects.begin(), subjects.end());
        m_subjectRepository->ConnectSubjectsToStudyGroups(listOfSubjects, studyGroup->m_id);
    }
    return true;
}

bool StudyGroupWidget::CanCreateNew(const Model *model)
{
    return !m_studyGroupRepository->Exists(Cast(model));
}

StudyGroup *StudyGroupWidget::Cast(const Model *model)
{
    return static_cast<StudyGroup*>(
        const_cast<Model*>(model)
    );
}

StudyGroupForm *StudyGroupWidget::Cast(const Form *form)
{
    return static_cast<StudyGroupForm*>(
        const_cast<Form*>(form)
    );
}
