#include "studygroupform.h"

StudyGroupForm::StudyGroupForm(
    StudyGroup* studyGroup,
    std::shared_ptr<SubjectRepository> subjectRepository,
    QWidget *parent)
    : Form(parent), m_studyGroup(studyGroup), m_subjectRepository(subjectRepository)
{
    auto nameValidatorBuilder = ComposedValidator::create().invalidCharacters(" ");
    const auto nameValidator = std::make_shared<ComposedValidator>(nameValidatorBuilder);

    // Required?
    AddTextEdit("Abbrevation", m_studyGroup->m_abbrevation);

    {
        const auto saveStudyForm = [this](const QString& text)
        {
            m_studyGroup->m_studyForm = EnumMapper::ToStudyFormEnum(text);
        };

        AddCombobox(
            "Study form",
            EnumMapper::ToString(m_studyGroup->m_studyForm),
            StudyFormValueList,
            saveStudyForm
        );
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("\\d+")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_studyGroup->m_enrolled = text.toInt();
        };
        AddTextEdit("Enrolled", QString::number(m_studyGroup->m_enrolled), saveInt, numberValidator);
    }

    {
        const auto saveSemester = [this](const QString& text)
        {
            m_studyGroup->m_semester = EnumMapper::ToSemesterEnum(text);
        };

        AddCombobox(
            "Semester",
            EnumMapper::ToString(m_studyGroup->m_semester),
            SemesterValueList,
            saveSemester
        );
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("\\d{4}")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_studyGroup->m_year = text.toInt();
        };
        AddTextEdit("Year", QString::number(m_studyGroup->m_year), saveInt, numberValidator);
    }

    {
        const auto saveLanguage = [this](const QString& text)
        {
            m_studyGroup->m_language = EnumMapper::ToLanguageEnum(text);
        };

        AddCombobox(
            "Language",
            EnumMapper::ToString(m_studyGroup->m_language),
            LanguageValueList,
            saveLanguage
        );
    }

    {
        const auto saveStudyType = [this](const QString& text)
        {
            m_studyGroup->m_type = EnumMapper::ToStudyTypeEnum(text);
        };

        AddCombobox(
            "Study type",
            EnumMapper::ToString(m_studyGroup->m_type),
            StudyTypeValueList,
            saveStudyType
        );
    }

    const auto existsSubjects = m_subjectRepository->Count();

    if (existsSubjects)
    {
        const auto subjets = m_subjectRepository->GetAll();
        const auto selectedSubjects = m_subjectRepository->GetSubjectsForStudyGroup(m_studyGroup->m_id);
        m_selectedSubjects = std::set<Id>(selectedSubjects.begin(), selectedSubjects.end());

        const auto controlTools = new QGroupBox("Control tools", this);
        const auto layout = new QVBoxLayout(controlTools);

        const auto studyGroupList = new QListWidget(this);

        const auto insertSubjectsToList = [this, &studyGroupList](const auto sg)
        {
            const auto qcb = new QCheckBox(sg.m_abbrevation, studyGroupList);
            qcb->setChecked(m_selectedSubjects.find(sg.m_id) != m_selectedSubjects.end());
            const auto item = new QListWidgetItem(studyGroupList);
            studyGroupList->addItem(item);
            studyGroupList->setItemWidget(item, qcb);

            connect(
                qcb, &QCheckBox::stateChanged,
                this, [this, qcb, sg]()
                {
                    if (qcb->isChecked())
                    {
                        m_selectedSubjects.insert(sg.m_id);
                    }
                    else
                    {
                        m_selectedSubjects.erase(sg.m_id);
                    }
                }
            );
        };

        std::for_each(
            subjets.begin(),
            subjets.end(),
            insertSubjectsToList
        );


        layout->addWidget(studyGroupList);

        m_layout->addRow(controlTools);
    }
    else
    {
        const auto warn = new QLabel("No subjects!\nAdd some subjects.");
        warn->setStyleSheet("font-weight: bold");
        m_layout->addRow(warn);
    }
}

std::set<Id> StudyGroupForm::GetSelectedSubjects()
{
    return m_selectedSubjects;
}
