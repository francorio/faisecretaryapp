#ifndef STUDYGROUPFORM_H
#define STUDYGROUPFORM_H

#include <QWidget>
#include <QCheckBox>
#include <QGroupBox>
#include <QListWidget>
#include "validator.h"
#include "Utils/form.h"
#include "Utils/enummapper.h"
#include "Models/studygroup.h"

#include "Repositories/subjectrepository.h"

class StudyGroupForm : public Form
{
    Q_OBJECT
public:
    explicit StudyGroupForm(
        StudyGroup* studyGroup,
        std::shared_ptr<SubjectRepository> subjectRepository,
        QWidget *parent = nullptr);
        std::set<Id> GetSelectedSubjects();
private:
    StudyGroup* m_studyGroup;
    std::shared_ptr<SubjectRepository> m_subjectRepository;

    std::set<Id> m_selectedSubjects;

};

#endif // STUDYGROUPFORM_H
