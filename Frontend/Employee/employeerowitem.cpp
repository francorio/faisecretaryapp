#include "employeerowitem.h"

EmployeeRowItem::EmployeeRowItem(
    const Employee *m,
    QWidget *parent
): RowItem(m, parent)
{
    const auto name = m->m_name;
    const auto surname = m->m_surname;

    const auto workEmail = m->m_workEmail;
    const auto workPhone = m->m_workPhone;

    m_layout->addWidget(new QLabel(name, this));
    m_layout->addWidget(new QLabel(surname, this));
    m_layout->addWidget(new QLabel(workEmail, this));
    m_layout->addWidget(new QLabel(workPhone, this));
}
