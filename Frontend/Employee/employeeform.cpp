#include "employeeform.h"
#include <QDebug>

static const auto PHONE_NUMBER_EXPRESSION = QString("(\\d{9}|\\d{3} \\d{3} \\d{3})");

EmployeeForm::EmployeeForm(
    Employee* employee,
    std::shared_ptr<WorkItemRepository> workItemRepository,
    QWidget *parent
):
    Form(parent),
    m_employee(employee),
    m_workItemRepository(workItemRepository)
{
    auto nameValidatorBuilder = ComposedValidator::create().invalidCharacters(" ");
    const auto nameValidator = std::make_shared<ComposedValidator>(nameValidatorBuilder);

    auto emailValidatorBuilder = ComposedValidator::create()
        .contains("@")
        .contains(".");

    const auto emailValidator = std::make_shared<ComposedValidator>(emailValidatorBuilder);
    const auto requiredEmailValidator = std::make_shared<ComposedValidator>(emailValidatorBuilder.required());

    auto numberValidatorBuilder = ComposedValidator::create().matchRegex(QRegularExpression(PHONE_NUMBER_EXPRESSION));

    const auto numberValidator = std::make_shared<ComposedValidator>(numberValidatorBuilder);
    const auto requiredNumberValidator = std::make_shared<ComposedValidator>(numberValidatorBuilder.required());


    AddTextEdit("Name", m_employee->m_name, nameValidator);
    AddTextEdit("Surname", m_employee->m_surname, nameValidator);
    AddTextEdit("Work email", m_employee->m_workEmail, requiredEmailValidator);
    AddTextEdit("Personal email", m_employee->m_personalEmail, emailValidator);
    AddTextEdit("Work phone", m_employee->m_workPhone, requiredNumberValidator);
    AddTextEdit("Personal phone", m_employee->m_personalPhone, numberValidator);

    {
        const auto checkbox = new QCheckBox(this);
        connect(
            checkbox, &QCheckBox::stateChanged,
            this, [this](const int state)
            {
                m_employee->m_isInStudy = state == Qt::Checked;
            }
        );

        checkbox->setCheckState(m_employee->m_isInStudy ? Qt::Checked : Qt::Unchecked);

        m_layout->addRow(new QLabel("PhD student", this), checkbox);
    }

    {
        auto floatNumberValidatorBuilder = ComposedValidator::create()
            .matchRegex(QRegularExpression("^[0-9]*(\\.[0-9]+)?$"))
            .required();
        const auto floatValidator = std::make_shared<ComposedValidator>(floatNumberValidatorBuilder);
        const auto saveValue = [this](const QString& text)
        {
            m_employee->m_workLoad = text.toFloat();
        };
        AddTextEdit("Work load", QString::number(m_employee->m_workLoad), saveValue, floatValidator);
    }

    {
        m_layout->addRow(
            new QLabel("Work points [CZ]", this),
            new QLabel(QString::number(m_employee->m_CZWorkPoints), this)
        );
    }

    {
        m_layout->addRow(
            new QLabel("Work points [EN]", this),
            new QLabel(QString::number(m_employee->m_ENWorkPoints), this)
        );
    }

    const auto existsWorkItems = m_workItemRepository->Count();

    if (existsWorkItems)
    {
        const auto workItems = m_workItemRepository->GetAll();
        const auto selectedWorkItems = m_workItemRepository->GetWorkItemsForEmployee(m_employee->m_id);
        m_selectedWorkItemIds = std::set<Id>(selectedWorkItems.begin(), selectedWorkItems.end());

        const auto controlTools = new QGroupBox("Assign work item", this);
        const auto layout = new QVBoxLayout(controlTools);

        const auto workItemList = new QListWidget(this);

        const auto insertWorkItemsToList = [this, &workItemList](const auto wi)
        {
            const auto qcb = new QCheckBox(wi.m_name, workItemList);
            qcb->setChecked(m_selectedWorkItemIds.find(wi.m_id) != m_selectedWorkItemIds.end());
            const auto item = new QListWidgetItem(workItemList);
            workItemList->addItem(item);
            workItemList->setItemWidget(item, qcb);

            connect(
                qcb, &QCheckBox::stateChanged,
                this, [this, qcb, wi]()
                {
                    if (qcb->isChecked())
                    {
                        m_selectedWorkItemIds.insert(wi.m_id);
                    }
                    else
                    {
                        m_selectedWorkItemIds.erase(wi.m_id);
                    }
                }
            );
        };

        std::for_each(
            workItems.begin(),
            workItems.end(),
            insertWorkItemsToList
        );


        layout->addWidget(workItemList);

        m_layout->addRow(controlTools);
    }
    else
    {
        const auto warn = new QLabel("No work items!\nAdd some work items.");
        warn->setStyleSheet("font-weight: bold");
        m_layout->addRow(warn);
    }

}

std::set<Id> EmployeeForm::GetSelectedWorkItems()
{
    return m_selectedWorkItemIds;
}
