#ifndef EMLOYEEWIDGET_H
#define EMLOYEEWIDGET_H

#include <QtWidgets>
#include "Utils/refreshablewidget.h"
#include "Repositories/employeerepository.h"
#include "Repositories/workitemrepository.h"
#include "Utils/programmabledialog.h"
#include "employeeform.h"
#include "Utils/itemwidget.h"
#include "employeerowitem.h"


class EmployeeWidget : public ItemWidget
{
    Q_OBJECT
public:
    explicit EmployeeWidget(
        std::shared_ptr<EmployeeRepository> employeeRepository,
        std::shared_ptr<WorkItemRepository> workItemRepository,
        QWidget *parent = nullptr
    );

protected:
    void Connect() override;
    void EnableOperationButtons(const bool b) override;
    void DBRefresh() override;
    void UIRefresh() override;
    void ClearContainer() override;
    void OnAddClicked() override;
    void OnEditClicked() override;
    void SaveSelectedObject() override;
    void ResetSelectedObject() override;
    void RemoveSelectedObject() override;

    bool OnAddNewObject(const Model *model, const Form *form) override;
    bool OnEditObject(const Model *model, const Form *form) override;
    bool CanCreateNew(const Model* model) override;

private:
    Employee* Cast(const Model* model);
    EmployeeForm* Cast(const Form* form);

    QList<Employee> m_employees;
    EmployeeRowItem* m_selectedEmployee;

    enum Buttons: int { CALCULATE = 0, COUNT };
    std::vector<QPushButton*> m_buttons;

    std::shared_ptr<EmployeeRepository> m_employeeRepository;
    std::shared_ptr<WorkItemRepository> m_workItemRepository;

    void OnCalculateClicked();

};

#endif // EMLOYEEWIDGET_H
