#ifndef EMPLOYEEROWITEM_H
#define EMPLOYEEROWITEM_H

#include "Utils/rowitem.h"
#include "Models/employee.h"
#include <QLabel>

class EmployeeRowItem: public RowItem
{
public:
    EmployeeRowItem(const Employee* m, QWidget* parent = nullptr);
};

#endif // EMPLOYEEROWITEM_H
