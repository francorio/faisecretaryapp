#ifndef EMPLOYEEFORM_H
#define EMPLOYEEFORM_H

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QToolTip>
#include <QGroupBox>
#include <QListWidget>
#include <QCheckBox>
#include "Utils/form.h"
#include "validator.h"

#include "Repositories/workitemrepository.h"
#include "Models/workitem.h"


#include "Models/employee.h"

class EmployeeForm : public Form
{
    Q_OBJECT
public:
    explicit EmployeeForm(
        Employee* e,
        std::shared_ptr<WorkItemRepository> workItemRepository,
        QWidget* parent = nullptr
    );

    std::set<Id> GetSelectedWorkItems();
private:
    Employee* m_employee;

    std::shared_ptr<WorkItemRepository> m_workItemRepository;
    std::set<Id> m_selectedWorkItemIds;
};

#endif // EMPLOYEEFORM_H
