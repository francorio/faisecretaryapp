#include "employeewidget.h"

EmployeeWidget::EmployeeWidget(
    std::shared_ptr<EmployeeRepository> employeeRepository,
    std::shared_ptr<WorkItemRepository> workItemRepository,
    QWidget *parent
):
    ItemWidget(parent),
    m_employeeRepository(employeeRepository),
    m_workItemRepository(workItemRepository)
{
    m_buttons.resize(Buttons::COUNT);

    m_buttons[Buttons::CALCULATE] = new QPushButton("Calculate work points", this);

    m_sidePanel->addWidget(m_buttons[Buttons::CALCULATE]);


    Connect();
    EnableOperationButtons(false);
    OnRefresh();
}

void EmployeeWidget::ClearContainer()
{
    m_list->clear();
    m_employees.clear();
}

void EmployeeWidget::DBRefresh()
{
    m_employees = m_employeeRepository->GetAll();
}

void EmployeeWidget::UIRefresh()
{
    for (const auto& f: m_employees)
    {
        const auto itemWidget = new EmployeeRowItem(&f, m_list);

        const auto listItem = new QListWidgetItem(m_list);
        listItem->setSizeHint(itemWidget->sizeHint());

        m_list->addItem(listItem);
        m_list->setItemWidget(listItem, itemWidget);
    }
}

void EmployeeWidget::Connect()
{
    ItemWidget::Connect();
    connect(m_buttons[Buttons::CALCULATE], &QPushButton::clicked, this, &EmployeeWidget::OnCalculateClicked);
}

void EmployeeWidget::OnAddClicked()
{
    const auto newEmployee = new Employee();
    const auto newEmployeeForm = new EmployeeForm(newEmployee, m_workItemRepository);

    const auto dialog = new ProgrammableDialog(newEmployeeForm, this);
    dialog->SetAcceptButtonText("Add");

    auto shouldRefresh = false;

    const auto acceptFunction = ProgrammableDialog::Action(
        [this, newEmployee, newEmployeeForm, &shouldRefresh]() -> bool
        {
            if (!newEmployeeForm->IsValid())
                return false;

            if (OnAddNewObject(newEmployee, newEmployeeForm))
            {
                shouldRefresh = true;
                return true;
            }
            else
            {
                QMessageBox mb(this);
                mb.setText("User already exists");
                mb.exec();
                return false;
            }
        }
    );

    dialog->OnAccept(acceptFunction);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }
}

void EmployeeWidget::OnEditClicked()
{
    if (!m_selectedItem)
    {
        return;
    }

    const auto index = m_employees.indexOf(Employee(m_selectedEmployee->m_id));
    if (index == -1)
    {
        qDebug() << "Subject not found!";
        return;
    }
    auto selected = &m_employees[index];

    const auto updateWorkItemForm = new EmployeeForm(selected, m_workItemRepository);

    const auto dialog = new ProgrammableDialog(updateWorkItemForm, this);
    dialog->SetAcceptButtonText("Update");

    auto shouldRefresh = false;

    const auto onUpdate = ProgrammableDialog::Action(
        [this, &shouldRefresh, selected, updateWorkItemForm]() -> bool
        {
            if (!updateWorkItemForm->IsValid())
                return false;

            if (OnEditObject(selected, updateWorkItemForm))
            {
                shouldRefresh = true;
                return true;
            }
            return false;
        }
    );

    dialog->OnAccept(onUpdate);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }
}

void EmployeeWidget::EnableOperationButtons(const bool b)
{
    ItemWidget::EnableOperationButtons(b);
    m_buttons[Buttons::CALCULATE]->setEnabled(b);
}

void EmployeeWidget::SaveSelectedObject()
{
    m_selectedEmployee = dynamic_cast<EmployeeRowItem*>(m_list->itemWidget(m_selectedItem));
}

void EmployeeWidget::ResetSelectedObject()
{
    ItemWidget::ResetSelectedObject();
    m_selectedEmployee = nullptr;
}

void EmployeeWidget::RemoveSelectedObject()
{
    {
        // UI list query
        const auto employeeToDelete = Employee(m_selectedEmployee->m_id);
        m_employees.removeOne(employeeToDelete);
    }
    {
        // Database query
        m_employeeRepository->Remove(m_selectedEmployee->m_id);
    }

    // Widget list query
    m_list->takeItem(m_list->currentRow());
}

bool EmployeeWidget::OnAddNewObject(const Model *model, const Form *form)
{
    const auto employee = static_cast<Employee*>(const_cast<Model*>(model));
    const auto employeeForm = static_cast<EmployeeForm*>(const_cast<Form*>(form));
    (void)employeeForm;

    const auto canCreateNewSubject = CanCreateNew(model);

    auto success = false;

    if (canCreateNewSubject)
    {
        m_employeeRepository->Add(employee);
        success = true;
    }
    return success;
}

bool EmployeeWidget::OnEditObject(const Model *model, const Form *form)
{
    const auto employee = Cast(model);
    const auto employeeForm = Cast(form);

    {
        const auto transaction = m_workItemRepository->GetTransaction();
        m_employeeRepository->Update(employee);
        const auto selectedWorkItems = employeeForm->GetSelectedWorkItems();
        const auto listOfWorkItems = std::list<Id>(selectedWorkItems.begin(), selectedWorkItems.end());
        m_workItemRepository->UpdateWorkItems(employee->m_id, listOfWorkItems);
    }
    return true;
}

bool EmployeeWidget::CanCreateNew(const Model *model)
{
    return !m_employeeRepository->Exists(Cast(model));
}

Employee *EmployeeWidget::Cast(const Model *model)
{
    return static_cast<Employee*>(
        const_cast<Model*>(model)
    );
}

EmployeeForm *EmployeeWidget::Cast(const Form *form)
{
    return static_cast<EmployeeForm*>(
        const_cast<Form*>(form)
                );
}

void EmployeeWidget::OnCalculateClicked()
{
    static std::map<WorkItemType, std::map<Language, int>> points{
        {
            WorkItemType::Lecture, {
                { Language::CZ, 12 },
                { Language::EN, 13 }
            }
        },
        {
            WorkItemType::Exercise, {
                { Language::CZ, 14 },
                { Language::EN, 15 }
            }
        },
        {
            WorkItemType::Seminar, {
                { Language::CZ, 11 },
                { Language::EN, 12 }
            }
        },
        {
            WorkItemType::Credit, {
                { Language::CZ, 15 },
                { Language::EN, 16 }
            }
        },
        {
            WorkItemType::ClassifiedCredit, {
                { Language::CZ, 15 },
                { Language::EN, 16 }
            }
        },
        {
            WorkItemType::Exam, {
                { Language::CZ, 17 },
                { Language::EN, 18 }
            }
        }
    };

    const auto workItems = m_workItemRepository->GetCompleteWorkItemsForEmployee(m_selectedEmployee->m_id);

    auto employee = m_employeeRepository->Get(m_selectedEmployee->m_id);

    employee.m_CZWorkPoints = employee.m_ENWorkPoints = 0;

    for (const auto& wi: workItems)
    {
        switch(wi.m_language)
        {
        case Language::CZ:
            employee.m_CZWorkPoints += points[wi.m_type][wi.m_language];
            break;
        case Language::EN:
            employee.m_ENWorkPoints += points[wi.m_type][wi.m_language];
        }
    }

    m_employeeRepository->Update(&employee);
    OnRefresh();
}

