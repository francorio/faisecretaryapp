#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include "Database/database.h"

#include <Utils/refreshablewidget.h>

#include "Employee/employeewidget.h"
#include "StudyGroup/studygroupwidget.h"
#include "Subject/subjectwidget.h"
#include "WorkItem/workitemwidget.h"

#include "Repositories/employeerepository.h"
#include "Repositories/studygrouprepository.h"
#include "Repositories/subjectrepository.h"
#include "Repositories/workitemrepository.h"



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void RefreshTab(const int index);
    QTabWidget* m_tabWidget;
    std::map<int, RefreshableWidget*> m_tabs;
    std::shared_ptr<Database> m_db;
};
#endif // MAINWINDOW_H
