#include "subjectrowitem.h"

SubjectRowItem::SubjectRowItem(
    const Subject* m,
    QWidget* parent
): RowItem(m, parent)
{
    const auto abbrevation = m->m_abbrevation;
    const auto title = m->m_title;
    const auto capacity = m->m_groupCapacity;
    const auto language = m->m_language;
    const auto completion = m->m_completion;

    m_layout->addWidget(new QLabel(abbrevation, this));
    m_layout->addWidget(new QLabel(title, this));
    m_layout->addWidget(new QLabel(EnumMapper::ToString(language), this));
    m_layout->addWidget(new QLabel(EnumMapper::ToString(completion), this));
    m_layout->addWidget(new QLabel(QString::number(capacity), this));
}
