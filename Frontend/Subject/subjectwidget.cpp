#include "subjectwidget.h"

SubjectWidget::SubjectWidget(
    std::shared_ptr<SubjectRepository> subjectRepository,
    std::shared_ptr<StudyGroupRepository> studyGroupRepository,
    QWidget *parent
)
    :ItemWidget(parent), m_subjectRepository(subjectRepository), m_studyGroupRepository(studyGroupRepository)
{
    Connect();

    EnableOperationButtons(false);

    OnRefresh();
}

void SubjectWidget::DBRefresh()
{
    m_subjects = m_subjectRepository->GetAll();
}

void SubjectWidget::UIRefresh()
{
    for (const auto& f: m_subjects)
    {

        auto listItem = new QListWidgetItem(m_list);

        const auto itemWidget = new SubjectRowItem(&f, m_list);

        listItem->setSizeHint(itemWidget->sizeHint());

        m_list->addItem(listItem);

        m_list->setItemWidget(listItem, itemWidget);
    }
}

void SubjectWidget::ClearContainer()
{
    m_list->clear();
    m_subjects.clear();
}

void SubjectWidget::OnAddClicked()
{
    const auto newSubject = new Subject();
    const auto newSubjectForm = new SubjectForm(newSubject, m_studyGroupRepository);

    const auto dialog = new ProgrammableDialog(newSubjectForm, this);
    dialog->SetAcceptButtonText("Add");

    auto shouldRefresh = false;

    const auto acceptFunction = ProgrammableDialog::Action(
        [this, newSubject, newSubjectForm, &shouldRefresh]() -> bool
        {
            if (!newSubjectForm->IsValid())
                return false;

            if (OnAddNewObject(newSubject, newSubjectForm))
            {
                shouldRefresh = true;
                return true;
            }
            else
            {
                QMessageBox mb(this);
                mb.setText("Subject already exists");
                mb.exec();
                return false;
            }
        }
    );

    dialog->OnAccept(acceptFunction);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }
}

void SubjectWidget::OnEditClicked()
{
    if (!m_selectedItem)
    {
        return;
    }

    const auto index = m_subjects.indexOf(Subject(m_selectedSubject->m_id));
    if (index == -1)
    {
        qDebug() << "Subject not found!";
        return;
    }
    auto selected = &m_subjects[index];

    const auto updateSubjectForm = new SubjectForm(selected, m_studyGroupRepository);

    const auto dialog = new ProgrammableDialog(updateSubjectForm, this);
    dialog->SetAcceptButtonText("Update");

    auto shouldRefresh = false;

    const auto onUpdate = ProgrammableDialog::Action(
        [this, &shouldRefresh, selected, updateSubjectForm]() -> bool
        {
            if (!updateSubjectForm->IsValid())
                return false;

            if (OnEditObject(selected, updateSubjectForm))
            {
                shouldRefresh = true;
                return true;
            }
            return false;
        }
    );

    dialog->OnAccept(onUpdate);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }
}

void SubjectWidget::SaveSelectedObject()
{
    m_selectedSubject = dynamic_cast<SubjectRowItem*>(m_list->itemWidget(m_selectedItem));
}

void SubjectWidget::ResetSelectedObject()
{
    ItemWidget::ResetSelectedObject();
    m_selectedSubject = nullptr;
}

void SubjectWidget::RemoveSelectedObject()
{
    {
        // UI list query
        const auto subjectToDelete = Subject(m_selectedSubject->m_id);
        m_subjects.removeOne(subjectToDelete);
    }
    {
        // Database query
        m_subjectRepository->Remove(m_selectedSubject->m_id);
    }

    // Widget list query
    m_list->takeItem(m_list->currentRow());
}

bool SubjectWidget::OnAddNewObject(const Model *model, const Form *form)
{
    const auto subject = Cast(model);
    const auto subjectForm = Cast(form);

    const auto canCreateNewSubject = CanCreateNew(model);

    auto success = false;

    if (canCreateNewSubject)
    {
        {
            const auto transaction = m_subjectRepository->GetTransaction();

            const auto subjectId = m_subjectRepository->Add(subject);
            const auto studyGroups = subjectForm->GetSelectedStudyGroups();
            const auto listOfStudyGroups = std::list<Id>(studyGroups.begin(), studyGroups.end());
            m_subjectRepository->ConnectSubjectsToStudyGroups(subjectId, listOfStudyGroups);
        }
        success = true;
    }

    return success;
}

bool SubjectWidget::OnEditObject(const Model *model, const Form *form)
{
    const auto subject = Cast(model);
    const auto subjectForm = Cast(form);

    {
        const auto transaction = m_subjectRepository->GetTransaction();
        m_subjectRepository->Update(subject);
        m_subjectRepository->DisconnectSubjectFromStudyGroups(subject);
        const auto studyGroups = subjectForm->GetSelectedStudyGroups();
        const auto listOfStudyGroups = std::list<Id>(studyGroups.begin(), studyGroups.end());
        m_subjectRepository->ConnectSubjectsToStudyGroups(subject->m_id, listOfStudyGroups);
    }
    return true;
}

bool SubjectWidget::CanCreateNew(const Model *model)
{
    return !m_subjectRepository->Exists(Cast(model));
}

Subject *SubjectWidget::Cast(const Model *model)
{
    return static_cast<Subject*>(
        const_cast<Model*>(model)
    );
}

SubjectForm *SubjectWidget::Cast(const Form *form)
{
    return static_cast<SubjectForm*>(
        const_cast<Form*>(form)
    );
}
