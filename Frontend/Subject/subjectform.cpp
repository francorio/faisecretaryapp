#include "subjectform.h"

SubjectForm::SubjectForm(Subject *subject, std::shared_ptr<StudyGroupRepository> studyGroupRepository, QWidget *parent)
    : Form(parent), m_subject(subject), m_studyGroupRepository(studyGroupRepository)
{
    auto nameValidatorBuilder = ComposedValidator::create().invalidCharacters(" ");
    const auto nameValidator = std::make_shared<ComposedValidator>(nameValidatorBuilder);

    AddTextEdit("Abbrevation", m_subject->m_abbrevation);
    AddTextEdit("Title", m_subject->m_title);

    {
        const auto saveCompletion = [this](const QString& text)
        {
            m_subject->m_completion = EnumMapper::ToCompletionEnum(text);
        };

        AddCombobox(
            "Completion",
            EnumMapper::ToString(m_subject->m_completion),
            CompletionValueList,
            saveCompletion
        );
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("^\\d{1,2}$")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_subject->m_weeks = text.toInt();
        };
        AddTextEdit("Weeks", QString::number(m_subject->m_weeks), saveInt, numberValidator);
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("^\\d{1,2}$")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_subject->m_lectureHours = text.toInt();
        };
        AddTextEdit("Lecture hours", QString::number(m_subject->m_lectureHours), saveInt, numberValidator);
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("^\\d{1,2}$")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_subject->m_seminarHours = text.toInt();
        };
        AddTextEdit("Seminar hours", QString::number(m_subject->m_seminarHours), saveInt, numberValidator);
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("^\\d{1,2}$")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_subject->m_exerciseHours = text.toInt();
        };
        AddTextEdit("Exercise hours", QString::number(m_subject->m_exerciseHours), saveInt, numberValidator);
    }

    {
        const auto saveLanguage = [this](const QString& text)
        {
            m_subject->m_language = EnumMapper::ToLanguageEnum(text);
        };

        AddCombobox(
            "Language",
            EnumMapper::ToString(m_subject->m_language),
            LanguageValueList,
            saveLanguage
        );
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("\\d{1,2}")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_subject->m_groupCapacity = text.toInt();
        };
        AddTextEdit("Group capacity", QString::number(m_subject->m_groupCapacity), saveInt, numberValidator);
    }

    const auto existsStudyGroups = m_studyGroupRepository->Count();

    if (existsStudyGroups)
    {
        const auto studyGroups = m_studyGroupRepository->GetAll();
        const auto selectedStudyGroups = m_studyGroupRepository->GetStudyGroupsForSubject(m_subject->m_id);
        m_selectedStudyGroupIds = std::set<Id>(selectedStudyGroups.begin(), selectedStudyGroups.end());

        const auto controlTools = new QGroupBox("Control tools", this);
        const auto layout = new QVBoxLayout(controlTools);

        const auto studyGroupList = new QListWidget(this);

        const auto insertStudyGroupsToList = [this, &studyGroupList](const auto sg)
        {
            const auto qcb = new QCheckBox(sg.m_abbrevation, studyGroupList);
            qcb->setChecked(m_selectedStudyGroupIds.find(sg.m_id) != m_selectedStudyGroupIds.end());
            const auto item = new QListWidgetItem(studyGroupList);
            studyGroupList->addItem(item);
            studyGroupList->setItemWidget(item, qcb);

            connect(
                qcb, &QCheckBox::stateChanged,
                this, [this, qcb, sg]()
                {
                    if (qcb->isChecked())
                    {
                        m_selectedStudyGroupIds.insert(sg.m_id);
                    }
                    else
                    {
                        m_selectedStudyGroupIds.erase(sg.m_id);
                    }
                }
            );
        };

        std::for_each(
            studyGroups.begin(),
            studyGroups.end(),
            insertStudyGroupsToList
        );


        layout->addWidget(studyGroupList);

        m_layout->addRow(controlTools);
    }
    else
    {
        const auto warn = new QLabel("No study groups!\nAdd some study groups.");
        warn->setStyleSheet("font-weight: bold");
        m_layout->addRow(warn);
    }
}

std::set<Id> SubjectForm::GetSelectedStudyGroups()
{
    return m_selectedStudyGroupIds;
}
