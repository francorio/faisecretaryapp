#ifndef SUBJECTFORM_H
#define SUBJECTFORM_H

#include <QWidget>
#include <QGroupBox>
#include <QListWidget>
#include <QCheckBox>
#include "validator.h"
#include "Utils/form.h"
#include "Utils/enummapper.h"
#include "Models/subject.h"
#include "Repositories/studygrouprepository.h"

class SubjectForm : public Form
{
    Q_OBJECT
public:
    explicit SubjectForm(Subject* subject, std::shared_ptr<StudyGroupRepository> studyGroupRepository, QWidget *parent = nullptr);
    std::set<Id> GetSelectedStudyGroups();
private:
    Subject* m_subject;
    std::shared_ptr<StudyGroupRepository> m_studyGroupRepository;
    std::set<Id> m_selectedStudyGroupIds;
};

#endif // SUBJECTFORM_H
