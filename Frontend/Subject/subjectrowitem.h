#ifndef SUBJECTROWITEM_H
#define SUBJECTROWITEM_H

#include "Utils/rowitem.h"
#include "Models/subject.h"
#include <QLabel>
#include "Utils/enummapper.h"

class SubjectRowItem: public RowItem
{
public:
    SubjectRowItem(const Subject* m, QWidget* parent = nullptr);
};
#endif // SUBJECTROWITEM_H
