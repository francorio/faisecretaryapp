#ifndef SUBJECTWIDGET_H
#define SUBJECTWIDGET_H

#include <QtWidgets>

#include "subjectform.h"
#include "Utils/programmabledialog.h"

#include "Utils/itemwidget.h"
#include "Repositories/subjectrepository.h"
#include "Repositories/studygrouprepository.h"
#include "subjectrowitem.h"

class SubjectWidget : public ItemWidget
{
    Q_OBJECT
public:
    explicit SubjectWidget(
        std::shared_ptr<SubjectRepository> subjectRepository,
        std::shared_ptr<StudyGroupRepository> studyGroupRepository,
        QWidget *parent = nullptr
    );

protected:
    void DBRefresh() override;
    void UIRefresh() override;
    void ClearContainer() override;
    void OnAddClicked() override;
    void OnEditClicked() override;
    void SaveSelectedObject() override;
    void ResetSelectedObject() override;
    void RemoveSelectedObject() override;

    bool OnAddNewObject(const Model *model, const Form *form) override;
    bool OnEditObject(const Model *model, const Form *form) override;
    bool CanCreateNew(const Model* model) override;

private:
    Subject* Cast(const Model* model);
    SubjectForm* Cast(const Form* form);

    QList<Subject> m_subjects;
    SubjectRowItem* m_selectedSubject;

    std::shared_ptr<SubjectRepository> m_subjectRepository;
    std::shared_ptr<StudyGroupRepository> m_studyGroupRepository;
};

#endif // SUBJECTWIDGET_H
