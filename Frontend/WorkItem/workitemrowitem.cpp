#include "workitemrowitem.h"

WorkItemRowItem::WorkItemRowItem(
    const WorkItem *m,
    QWidget *parent
): RowItem(m, parent)
{
    const auto name = m->m_name.mid(0, m->m_name.lastIndexOf("_"));
    const auto type = m->m_type;

    m_layout->addWidget(new QLabel(name, this));
    m_layout->addWidget(new QLabel(EnumMapper::ToString(type), this));

    if (m->m_numberOfStudents == 0)
    {
        setStyleSheet("background-color: red");
    }
}
