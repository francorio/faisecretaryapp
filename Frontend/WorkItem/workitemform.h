#ifndef WORKITEMFORM_H
#define WORKITEMFORM_H

#include "Utils/form.h"
#include "validator.h"
#include "Utils/enummapper.h"
#include "Models/workitem.h"
#include "Repositories/workitemrepository.h"
#include "Repositories/employeerepository.h"
#include "Repositories/subjectrepository.h"


class WorkItemForm: public Form
{
    Q_OBJECT
public:
    explicit WorkItemForm(
        WorkItem* workItem,
        std::shared_ptr<WorkItemRepository> workItemRepository,
        std::shared_ptr<EmployeeRepository> employeeRepository,
        std::shared_ptr<SubjectRepository> subjectRepository,
        QWidget *parent = nullptr
    );
private:
    WorkItem* m_workItem;
    std::shared_ptr<WorkItemRepository> m_workItemRepository;
    std::shared_ptr<EmployeeRepository> m_employeeRepository;
    std::shared_ptr<SubjectRepository> m_subjectRepository;
};

#endif // WORKITEMFORM_H
