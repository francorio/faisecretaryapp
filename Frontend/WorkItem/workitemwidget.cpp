#include "workitemwidget.h"
#include "generatorlogic.h"

WorkItemWidget::WorkItemWidget(
    std::shared_ptr<WorkItemRepository> workItemRepository,
    std::shared_ptr<EmployeeRepository> employeeRepository,
    std::shared_ptr<SubjectRepository> subjectRepository,
    std::shared_ptr<StudyGroupRepository> studyGroupRepository,
    QWidget *parent
):
    ItemWidget(parent),
    m_workItemRepository(workItemRepository),
    m_employeeRepository(employeeRepository),
    m_subjectRepository(subjectRepository),
    m_studyGroupRepository(studyGroupRepository)
{
    m_buttons.resize(Buttons::COUNT);
    m_buttons[Buttons::GENERATE] = new QPushButton("Generate work items", this);

    m_sidePanel->addWidget(m_buttons[Buttons::GENERATE]);

    Connect();

    EnableOperationButtons(false);

    OnRefresh();
}

void WorkItemWidget::Connect()
{
    ItemWidget::Connect();

    connect(
        m_buttons[Buttons::GENERATE], &QPushButton::clicked,
        this, &WorkItemWidget::OnGenerateClicked
    );
}

void WorkItemWidget::DBRefresh()
{
    m_workItems = m_workItemRepository->GetAll();
}

void WorkItemWidget::ClearContainer()
{
    m_list->clear();
    m_workItems.clear();
}

void WorkItemWidget::UIRefresh()
{
    for (const auto& f: m_workItems)
    {
        auto listItem = new QListWidgetItem(m_list);

        const auto itemWidget = new WorkItemRowItem(&f, m_list);

        listItem->setSizeHint(itemWidget->sizeHint());

        m_list->addItem(listItem);

        m_list->setItemWidget(listItem, itemWidget);
    }
}

void WorkItemWidget::OnAddClicked()
{
    const auto newWorkItem = new WorkItem();
    const auto newWorkItemForm = new WorkItemForm(
        newWorkItem,
        m_workItemRepository,
        m_employeeRepository,
        m_subjectRepository
    );

    const auto dialog = new ProgrammableDialog(newWorkItemForm, this);
    dialog->SetAcceptButtonText("Add");

    auto shouldRefresh = false;

    const auto acceptFunction = ProgrammableDialog::Action(
        [this, newWorkItem, newWorkItemForm, &shouldRefresh]() -> bool
        {
            if (!newWorkItemForm->IsValid())
                return false;

            if (OnAddNewObject(newWorkItem, newWorkItemForm))
            {
                shouldRefresh = true;
                return true;
            }
            else
            {
                QMessageBox mb(this);
                mb.setText("Work item already exists");
                mb.exec();
                return false;
            }
        }
    );

    dialog->OnAccept(acceptFunction);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }
}

void WorkItemWidget::OnEditClicked()
{
    if (!m_selectedItem)
    {
        return;
    }

    const auto index = m_workItems.indexOf(WorkItem(m_selectedWorkItem->m_id));
    if (index == -1)
    {
        qDebug() << "Subject not found!";
        return;
    }
    auto selected = &m_workItems[index];

    const auto updateWorkItemForm = new WorkItemForm(
        selected,
        m_workItemRepository,
        m_employeeRepository,
        m_subjectRepository
    );

    const auto dialog = new ProgrammableDialog(updateWorkItemForm, this);
    dialog->SetAcceptButtonText("Update");

    auto shouldRefresh = false;

    const auto onUpdate = ProgrammableDialog::Action(
        [this, &shouldRefresh, selected, updateWorkItemForm]() -> bool
        {
            if (!updateWorkItemForm->IsValid())
                return false;

            if (OnEditObject(selected, updateWorkItemForm))
            {
                shouldRefresh = true;
                return true;
            }
            return false;
        }
    );

    dialog->OnAccept(onUpdate);

    dialog->exec();

    if (shouldRefresh)
    {
        OnRefresh();
    }
}

void WorkItemWidget::SaveSelectedObject()
{
    m_selectedWorkItem = dynamic_cast<WorkItemRowItem*>(m_list->itemWidget(m_selectedItem));
}

void WorkItemWidget::ResetSelectedObject()
{
    ItemWidget::ResetSelectedObject();
    m_selectedWorkItem = nullptr;
}

void WorkItemWidget::RemoveSelectedObject()
{
    {
        // UI list query
        const auto subjectToDelete = WorkItem(m_selectedWorkItem->m_id);
        m_workItems.removeOne(subjectToDelete);
    }
    {
        // Database query
        m_workItemRepository->Remove(m_selectedWorkItem->m_id);
    }

    // Widget list query
    m_list->takeItem(m_list->currentRow());
}

bool WorkItemWidget::OnAddNewObject(const Model *model, const Form *form)
{
    const auto workItem = Cast(model);
    const auto workItemForm = Cast(form);

    (void) workItemForm;

    const auto canCreateNewWorkItem = CanCreateNew(model);

    auto success = false;

    if (canCreateNewWorkItem)
    {
        m_workItemRepository->Add(workItem);
        success = true;
    }
    return success;
}

bool WorkItemWidget::OnEditObject(const Model *model, const Form *form)
{
    const auto workItem = Cast(model);
    const auto workItemForm = Cast(form);

    (void) workItemForm;

    {
        m_workItemRepository->Update(workItem);
    }
    return true;
}

bool WorkItemWidget::CanCreateNew(const Model *model)
{
    return !m_workItemRepository->Exists(
        static_cast<WorkItem*>(
            const_cast<Model*>(model)
        )
    );
}

WorkItem *WorkItemWidget::Cast(const Model *model)
{
    return static_cast<WorkItem*>(
        const_cast<Model*>(model)
    );
}

WorkItemForm *WorkItemWidget::Cast(const Form *form)
{
    return static_cast<WorkItemForm*>(
        const_cast<Form*>(form)
    );
}

void WorkItemWidget::OnGenerateClicked()
{
    qDebug() << "On generate clicked";
    const auto studyGroups = m_studyGroupRepository->GetAll();
    for (const auto& group: studyGroups)
    {
        const auto sub = m_subjectRepository->GetCompleteSubjectsForStudyGroup(group.m_id);
        const auto subjects = std::vector<Subject>(sub.begin(), sub.end());

        auto subjectIds = std::list<Id>();
        std::transform(subjects.begin(), subjects.end(), std::back_inserter(subjectIds), [](const auto& sub) { return sub.m_id; });
        const auto wi = m_workItemRepository->GetWorkItemsForSubjects(subjectIds);
        const auto workItems = std::vector<WorkItem>(wi.begin(), wi.end());
        const auto generated = GeneratorLogic::Generate(group, subjects, workItems);
        for (const auto& wi: generated)
        {
            if (wi.m_id)
                m_workItemRepository->Update(&wi);
            else
                m_workItemRepository->Add(&wi);

        }
    }
    OnRefresh();
}



