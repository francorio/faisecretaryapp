#include "workitemform.h"

WorkItemForm::WorkItemForm(
    WorkItem *workItem,
    std::shared_ptr<WorkItemRepository> workItemRepository,
    std::shared_ptr<EmployeeRepository> employeeRepository,
    std::shared_ptr<SubjectRepository> subjectRepository,
    QWidget *parent
):
    Form(parent),
    m_workItem(workItem),
    m_workItemRepository(workItemRepository),
    m_employeeRepository(employeeRepository),
    m_subjectRepository(subjectRepository)
{
    AddTextEdit("Name", m_workItem->m_name);

    {
        const auto saveWorkItemType = [this](const QString& text)
        {
            m_workItem->m_type = EnumMapper::ToWorkItemTypeEnum(text);
        };

        AddCombobox(
            "Work item type",
            EnumMapper::ToString(m_workItem->m_type),
            WorkItemTypeValueList,
            saveWorkItemType
        );
    }

    {
        const auto employeeCombo = new QComboBox(this);

        employeeCombo->addItem("None", 0);

        const auto allEmployees = m_employeeRepository->GetAll();

        auto data = std::vector<std::pair<int, Id>>();

        for (const auto& emp: allEmployees)
        {
            const auto name = emp.m_name;
            const auto surname = emp.m_surname;

            const auto text = name + " " + surname;

            employeeCombo->addItem(text, emp.m_id);
        }

        const auto index = employeeCombo->findData(m_workItem->m_employeeId);

        employeeCombo->setCurrentIndex(index != -1 ? index : 0);

        connect(
            employeeCombo, &QComboBox::currentTextChanged,
            this, [this, employeeCombo]()
            {
                m_workItem->m_employeeId = employeeCombo->currentData().toInt();
            }
        );

        m_layout->addRow(new QLabel("Employee", this), employeeCombo);
    }

    {
        const auto subjectCombo = new QComboBox(this);

        subjectCombo->addItem("None", 0);

        const auto allSubjects = m_subjectRepository->GetAll();

        auto data = std::vector<std::pair<int, Id>>();

        for (const auto& sub: allSubjects)
        {

            const auto text = sub.m_abbrevation + " - " + sub.m_title;

            subjectCombo->addItem(text, sub.m_id);
        }

        const auto index = subjectCombo->findData(m_workItem->m_subjectId);

        subjectCombo->setCurrentIndex(index != -1 ? index : 0);

        connect(
            subjectCombo, &QComboBox::currentTextChanged,
            this, [this, subjectCombo]()
            {
                m_workItem->m_subjectId = subjectCombo->currentData().toInt();
            }
        );

        m_layout->addRow(new QLabel("Subject", this), subjectCombo);

    }


    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("\\d{1,4}")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_workItem->m_numberOfStudents = text.toInt();
        };
        AddTextEdit("Number of students", QString::number(m_workItem->m_numberOfStudents), saveInt, numberValidator);
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("\\d{1,4}")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_workItem->m_numberOfHours = text.toInt();
        };
        AddTextEdit("Number of hours", QString::number(m_workItem->m_numberOfHours), saveInt, numberValidator);
    }

    {
        auto builder = ComposedValidator::create().matchRegex(QRegularExpression("\\d{1,4}")).required();
        const auto numberValidator = std::make_shared<ComposedValidator>(builder);
        const auto saveInt = [this](const QString& text)
        {
            m_workItem->m_numberOfWeeks = text.toInt();
        };
        AddTextEdit("Number of weeks", QString::number(m_workItem->m_numberOfWeeks), saveInt, numberValidator);
    }


    {
        const auto saveLanguage = [this](const QString& text)
        {
            m_workItem->m_language = EnumMapper::ToLanguageEnum(text);
        };

        AddCombobox(
            "Language",
            EnumMapper::ToString(m_workItem->m_language),
            LanguageValueList,
            saveLanguage
        );
    }

}
