#ifndef WORKITEMWIDGET_H
#define WORKITEMWIDGET_H

#include <QtWidgets>

#include "workitemform.h"
#include "Utils/programmabledialog.h"

#include "Utils/itemwidget.h"
#include "Repositories/workitemrepository.h"
#include "Repositories/employeerepository.h"
#include "Repositories/studygrouprepository.h"

#include "workitemrowitem.h"

class WorkItemWidget : public ItemWidget
{
    Q_OBJECT
public:
    explicit WorkItemWidget(
        std::shared_ptr<WorkItemRepository> workItemRepository,
        std::shared_ptr<EmployeeRepository> employeeRepository,
        std::shared_ptr<SubjectRepository> subjectRepository,
        std::shared_ptr<StudyGroupRepository> studyGroupRepository,
        QWidget *parent = nullptr
    );

protected:
    void Connect() override;
    void DBRefresh() override;
    void UIRefresh() override;
    void ClearContainer() override;
    void OnAddClicked() override;
    void OnEditClicked() override;
    void SaveSelectedObject() override;
    void ResetSelectedObject() override;
    void RemoveSelectedObject() override;

    bool OnAddNewObject(const Model *model, const Form *form) override;
    bool OnEditObject(const Model *model, const Form *form) override;
    bool CanCreateNew(const Model* model) override;

private:
    WorkItem* Cast(const Model* model);
    WorkItemForm* Cast(const Form* form);

    void OnGenerateClicked();

    QList<WorkItem> m_workItems;
    WorkItemRowItem* m_selectedWorkItem;

    std::shared_ptr<WorkItemRepository> m_workItemRepository;
    std::shared_ptr<EmployeeRepository> m_employeeRepository;
    std::shared_ptr<SubjectRepository> m_subjectRepository;
    std::shared_ptr<StudyGroupRepository> m_studyGroupRepository;

    enum Buttons: int { GENERATE = 0, COUNT };
    std::vector<QPushButton*> m_buttons;
};

#endif // WORKITEMWIDGET_H
