#ifndef WORKITEMROWITEM_H
#define WORKITEMROWITEM_H

#include "Utils/rowitem.h"
#include "Models/workitem.h"
#include <QLabel>
#include "Utils/enummapper.h"

class WorkItemRowItem: public RowItem
{
public:
    WorkItemRowItem(const WorkItem* m, QWidget* parent = nullptr);
};

#endif // WORKITEMROWITEM_H
